if CLIENT then
SWEP.WepSelectIcon = surface.GetTextureID( "sprites/hud/dods_smoke_ger" )
SWEP.DrawWeaponInfoBox	= false
SWEP.BounceWeaponIcon = false
killicon.Add( "weapon_smoke_ger", "sprites/hud/dods_smoke_ger", Color( 255, 255, 255, 255 ) )
killicon.Add( "ent_smoke_ger", "sprites/hud/dods_smoke_ger", Color( 255, 255, 255, 255 ) )
end

SWEP.PrintName = "Smoke Grenade"
SWEP.Category = "Day of Defeat: Source"
SWEP.Spawnable= true
SWEP.AdminSpawnable= true
SWEP.AdminOnly = false

SWEP.ViewModelFOV = 45
SWEP.ViewModel = "models/weapons/c_smoke_ger.mdl"
SWEP.WorldModel = "models/weapons/p_smoke_ger.mdl"
SWEP.ViewModelFlip = false

SWEP.AutoSwitchTo = false
SWEP.AutoSwitchFrom = false
SWEP.Weight = 0
SWEP.Slot = 4
SWEP.SlotPos = 1

SWEP.UseHands = true
SWEP.HoldType = "grenade"
SWEP.FiresUnderwater = true
SWEP.DrawCrosshair = false
SWEP.DrawAmmo = true
SWEP.CSMuzzleFlashes = 1
SWEP.Base = "weapon_base"

SWEP.Throw = 0
SWEP.ThrowTimer = CurTime()
SWEP.Sprint = 0
SWEP.SprintAngles = 0
SWEP.Idle = 0
SWEP.IdleTimer = CurTime()

SWEP.Primary.Sound = Sound( "Weapon_Grenade.Throw" )
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = 1
SWEP.Primary.MaxAmmo = 2
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "Grenade"
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.Delay = 0.2
SWEP.Primary.Force = 750

SWEP.Secondary.ClipSize = 0
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

function SWEP:Initialize()
self:SetHoldType( self.HoldType )
self.Idle = 0
self.IdleTimer = CurTime() + 1
end

if CLIENT then
function SWEP:GetViewModelPosition( pos, ang )
pos = pos + ang:Forward() * 6 + ang:Right() * 0 + ang:Up() * 0
ang:RotateAroundAxis( ang:Right(), self.Weapon:GetNWString( "SprintAngles", self.SprintAngles ) )
return pos, ang
end
end

function SWEP:Deploy()
self:SetHoldType( self.HoldType )
self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
self:SetNextPrimaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
self:SetNextSecondaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
self.Throw = 0
self.ThrowTimer = CurTime()
self.Sprint = 0
self.SprintAngles = 0
self.Idle = 0
self.IdleTimer = CurTime() + self.Owner:GetViewModel():SequenceDuration()
if self.Weapon:Ammo1() <= 0 then
if SERVER then
self.Owner:DropWeapon( self.Weapon )
end
self.Weapon:Remove()
end
return true
end

function SWEP:Holster()
if !( self.Throw == 3 ) then
self.Throw = 0
self.ThrowTimer = CurTime()
end
self.Sprint = 0
self.SprintAngles = 0
self.Idle = 0
self.IdleTimer = CurTime()
if self.Throw == 3 then
self.Weapon:Remove()
end
return true
end

function SWEP:PrimaryAttack()
if !( self.Throw == 0 ) then return end
if self.Sprint == 1 then return end
if self.Weapon:Ammo1() <= 0 then return end
self.Weapon:SendWeaponAnim( ACT_VM_PULLPIN )
self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
self:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
self.Throw = 1
self.ThrowTimer = CurTime() + self.Primary.Delay
self.Idle = 1
self.IdleTimer = CurTime()
end

function SWEP:SecondaryAttack()
end

function SWEP:Reload()
end

function SWEP:Think()
if self.Throw == 1 and self.ThrowTimer <= CurTime() and !self.Owner:KeyDown( IN_ATTACK ) then
if SERVER then
local entity = ents.Create( "ent_smoke_ger" )
entity:SetOwner( self.Owner )
if IsValid( entity ) then
local Forward = self.Owner:EyeAngles():Forward()
local Right = self.Owner:EyeAngles():Right()
local Up = self.Owner:EyeAngles():Up()
entity:SetPos( self.Owner:GetShootPos() + Forward * 4 + Right * 2 + Up * 2 )
entity:SetAngles( self.Owner:EyeAngles() )
entity:Spawn()
local phys = entity:GetPhysicsObject()
phys:SetVelocity( self.Owner:GetAimVector() * self.Primary.Force )
phys:AddAngleVelocity( Vector( math.random( -200, 200 ), math.random( -1000, 1000 ), math.random( -200, 200 ) ) )
end
end
self:EmitSound( self.Primary.Sound )
self:TakePrimaryAmmo( self.Primary.TakeAmmo )
self.Owner:SetAnimation( PLAYER_ATTACK1 )
if self.Weapon:Ammo1() > 0 then
self.Throw = 2
end
if self.Weapon:Ammo1() <= 0 then
self.Throw = 3
end
end
if self.Throw == 2 then
self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
self:SetNextPrimaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
self:SetNextSecondaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
self.Throw = 0
self.Idle = 0
self.IdleTimer = CurTime() + self.Owner:GetViewModel():SequenceDuration()
end
if self.Sprint == 0 and self.SprintAngles < 0 then
self.SprintAngles = self.SprintAngles + 3
end
if self.Sprint == 1 and self.SprintAngles > -30 then
self.SprintAngles = self.SprintAngles - 3
end
self.Weapon:SetNWString( "SprintAngles", self.SprintAngles )
if self.Sprint == 0 and self.Owner:KeyDown( IN_SPEED ) and ( self.Owner:KeyDown( IN_FORWARD ) || self.Owner:KeyDown( IN_BACK ) || self.Owner:KeyDown( IN_MOVELEFT ) || self.Owner:KeyDown( IN_MOVERIGHT ) ) then
self.Sprint = 1
self.SprintAngles = 0
end
if self.Sprint == 1 and !self.Owner:KeyDown( IN_SPEED ) then
self.Sprint = 0
end
if self.Sprint == 1 and !( self.Owner:KeyDown( IN_FORWARD ) || self.Owner:KeyDown( IN_BACK ) || self.Owner:KeyDown( IN_MOVELEFT ) || self.Owner:KeyDown( IN_MOVERIGHT ) ) then
self.Sprint = 0
end
if self.Idle == 0 and self.IdleTimer < CurTime() then
if SERVER then
self.Weapon:SendWeaponAnim( ACT_VM_IDLE )
end
self.Idle = 1
end
if self.Weapon:Ammo1() > self.Primary.MaxAmmo then
self.Owner:SetAmmo( self.Primary.MaxAmmo, self.Primary.Ammo )
end
if self.Throw == 3 then
if SERVER then
self.Owner:DropWeapon( self.Weapon )
end
self.Weapon:Remove()
end
end

function SWEP:DrawHUD()
local x = ScrW() / 2
local y = ScrH() / 2
surface.SetDrawColor( 255, 0, 0, 255 )
surface.DrawLine( x - 16, y, x - 8, y )
surface.DrawLine( x + 16, y, x + 8, y )
surface.DrawLine( x, y - 16, x, y - 8 )
surface.DrawLine( x, y + 16, x, y + 8 )
end