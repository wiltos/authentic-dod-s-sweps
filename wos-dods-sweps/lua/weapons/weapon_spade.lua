if CLIENT then
SWEP.WepSelectIcon = surface.GetTextureID( "sprites/hud/dods_spade" )
SWEP.DrawWeaponInfoBox	= false
SWEP.BounceWeaponIcon = false
killicon.Add( "weapon_spade", "sprites/hud/dods_spade", Color( 255, 255, 255, 255 ) )
end

SWEP.PrintName = "Spade"
SWEP.Category = "Day of Defeat: Source"
SWEP.Spawnable= true
SWEP.AdminSpawnable= true
SWEP.AdminOnly = false

SWEP.ViewModelFOV = 45
SWEP.ViewModel = "models/weapons/c_spade.mdl"
SWEP.WorldModel = "models/weapons/w_spade.mdl"
SWEP.ViewModelFlip = false

SWEP.AutoSwitchTo = false
SWEP.AutoSwitchFrom = false
SWEP.Weight = 5
SWEP.Slot = 0
SWEP.SlotPos = 1

SWEP.UseHands = true
SWEP.HoldType = "dods-spade-idle"
SWEP.FiresUnderwater = true
SWEP.DrawCrosshair = false
SWEP.DrawAmmo = true
SWEP.CSMuzzleFlashes = 1
SWEP.Base = "weapon_base"

SWEP.Attack = 0
SWEP.AttackTimer = CurTime()
SWEP.Sprint = 0
SWEP.SprintAngles = 0
SWEP.Idle = 0
SWEP.IdleTimer = CurTime()

SWEP.Primary.Sound = Sound( "Weapon_Knife.Swing" )
SWEP.Primary.ClipSize = 0
SWEP.Primary.DefaultClip = 0
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "none"
SWEP.Primary.Damage = 60
SWEP.Primary.DamageBackstab = 300
SWEP.Primary.Delay = 0.4
SWEP.Primary.DelayBackstab = 1.4
SWEP.Primary.Force = 1000

SWEP.Secondary.ClipSize = 0
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

function SWEP:Initialize()
self:SetHoldType( self.HoldType )
self.Idle = 0
self.IdleTimer = CurTime() + 1
end

if CLIENT then
function SWEP:GetViewModelPosition( pos, ang )
pos = pos + ang:Forward() * 3 + ang:Right() * 1 + ang:Up() * 0
ang:RotateAroundAxis( ang:Right(), self.Weapon:GetNWString( "SprintAngles", self.SprintAngles ) )
return pos, ang
end
end

function SWEP:Deploy()
self:SetHoldType( self.HoldType )
self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
self:SetNextPrimaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
self:SetNextSecondaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
self.Attack = 0
self.AttackTimer = CurTime()
self.Sprint = 0
self.SprintAngles = 0
self.Idle = 0
self.IdleTimer = CurTime() + self.Owner:GetViewModel():SequenceDuration()
return true
end

function SWEP:Holster()
self.Attack = 0
self.AttackTimer = CurTime()
self.Sprint = 0
self.SprintAngles = 0
self.Idle = 0
self.IdleTimer = CurTime()
return true
end

function SWEP:PrimaryAttack()
if self.Sprint == 1 then return end
local tr = util.TraceLine( {
start = self.Owner:GetShootPos(),
endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * 72,
filter = self.Owner,
mask = MASK_SHOT_HULL,
} )
if !IsValid( tr.Entity ) then
tr = util.TraceHull( {
start = self.Owner:GetShootPos(),
endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * 72,
filter = self.Owner,
mins = Vector( -16, -16, 0 ),
maxs = Vector( 16, 16, 0 ),
mask = MASK_SHOT_HULL,
} )
end
self:EmitSound( self.Primary.Sound )
self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
self:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
self.Attack = 1
self.AttackTimer = CurTime() + 0.2
if IsValid( tr.Entity ) then
local angle = self.Owner:GetAngles().y - tr.Entity:GetAngles().y
if angle < -180 then
angle = 360 + angle
end
if angle <= 90 and angle >= -90 and tr.Entity:IsNPC() || tr.Entity:IsPlayer() then
self.Weapon:SendWeaponAnim( ACT_VM_SECONDARYATTACK )
self:SetNextPrimaryFire( CurTime() + self.Primary.DelayBackstab )
self:SetNextSecondaryFire( CurTime() + self.Primary.DelayBackstab )
self.Attack = 2
self.AttackTimer = CurTime() + 0.4
end
end
self.Owner:SetAnimation( PLAYER_ATTACK1 )
self.Idle = 0
self.IdleTimer = CurTime() + self.Owner:GetViewModel():SequenceDuration()
end

function SWEP:SecondaryAttack()
end

function SWEP:Reload()
end

function SWEP:Think()
local tr = util.TraceLine( {
start = self.Owner:GetShootPos(),
endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * 72,
filter = self.Owner,
mask = MASK_SHOT_HULL,
} )
if !IsValid( tr.Entity ) then
tr = util.TraceHull( {
start = self.Owner:GetShootPos(),
endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * 72,
filter = self.Owner,
mins = Vector( -16, -16, 0 ),
maxs = Vector( 16, 16, 0 ),
mask = MASK_SHOT_HULL,
} )
end
if self.Attack == 1 and self.AttackTimer <= CurTime() then
if SERVER then
if tr.Hit and !( tr.Entity:IsNPC() || tr.Entity:IsPlayer() ) then
self.Owner:EmitSound( "Weapon_Spade.HitWorld" )
end
if IsValid( tr.Entity ) then
local dmginfo = DamageInfo()
local attacker = self.Owner
if !IsValid( attacker ) then
attacker = self
end
dmginfo:SetAttacker( attacker )
dmginfo:SetInflictor( self )
dmginfo:SetDamage( self.Primary.Damage )
dmginfo:SetDamageForce( self.Owner:GetForward() * self.Primary.Force )
tr.Entity:TakeDamageInfo( dmginfo )
if tr.Hit then
if tr.Entity:IsNPC() || tr.Entity:IsPlayer() then
self.Owner:EmitSound( "Weapon_Spade.HitPlayer" )
end
if !( tr.Entity:IsNPC() || tr.Entity:IsPlayer() ) then
self.Owner:EmitSound( "Weapon_Spade.HitWorld" )
end
end
end
end
self.Attack = 0
end
if self.Attack == 2 and self.AttackTimer <= CurTime() then
if SERVER then
if tr.Hit and !( tr.Entity:IsNPC() || tr.Entity:IsPlayer() ) then
self.Owner:EmitSound( "Weapon_Spade.HitWorld" )
end
if IsValid( tr.Entity ) then
local dmginfo = DamageInfo()
local attacker = self.Owner
if !IsValid( attacker ) then
attacker = self
end
dmginfo:SetAttacker( attacker )
dmginfo:SetInflictor( self )
local angle = self.Owner:GetAngles().y - tr.Entity:GetAngles().y
if angle < -180 then
angle = 360 + angle
end
if angle <= 90 and angle >= -90 and tr.Entity:IsNPC() || tr.Entity:IsPlayer() then
dmginfo:SetDamage( self.Primary.DamageBackstab )
else
dmginfo:SetDamage( self.Primary.Damage )
end
dmginfo:SetDamageForce( self.Owner:GetForward() * self.Primary.Force )
tr.Entity:TakeDamageInfo( dmginfo )
if tr.Hit then
if tr.Entity:IsNPC() || tr.Entity:IsPlayer() then
self.Owner:EmitSound( "Weapon_Spade.HitPlayer" )
end
if !( tr.Entity:IsNPC() || tr.Entity:IsPlayer() ) then
self.Owner:EmitSound( "Weapon_Spade.HitWorld" )
end
end
end
end
self.Attack = 0
end
if self.Sprint == 0 and self.SprintAngles < 0 then
self.SprintAngles = self.SprintAngles + 3
end
if self.Sprint == 1 and self.SprintAngles > -30 then
self.SprintAngles = self.SprintAngles - 3
end
self.Weapon:SetNWString( "SprintAngles", self.SprintAngles )
if self.Sprint == 0 and self.Owner:KeyDown( IN_SPEED ) and ( self.Owner:KeyDown( IN_FORWARD ) || self.Owner:KeyDown( IN_BACK ) || self.Owner:KeyDown( IN_MOVELEFT ) || self.Owner:KeyDown( IN_MOVERIGHT ) ) then
self.Sprint = 1
self.SprintAngles = 0
end
if self.Sprint == 1 and !self.Owner:KeyDown( IN_SPEED ) then
self.Sprint = 0
end
if self.Sprint == 1 and !( self.Owner:KeyDown( IN_FORWARD ) || self.Owner:KeyDown( IN_BACK ) || self.Owner:KeyDown( IN_MOVELEFT ) || self.Owner:KeyDown( IN_MOVERIGHT ) ) then
self.Sprint = 0
end
if self.Idle == 0 and self.IdleTimer < CurTime() then
if SERVER then
self.Weapon:SendWeaponAnim( ACT_VM_IDLE )
end
self.Idle = 1
end
end