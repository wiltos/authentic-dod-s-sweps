SWEP.PrintName = "WOS DOD:S WEAPON BASE"
SWEP.Category = "wiltOS Technologies"
SWEP.Spawnable= false
SWEP.AdminSpawnable= false
SWEP.AdminOnly = false

SWEP.ViewModelFOV = 45
SWEP.ViewModel = "models/weapons/c_mp40.mdl"
SWEP.WorldModel = "models/weapons/w_mp40.mdl"
SWEP.ViewModelFlip = false

SWEP.AutoSwitchTo = false
SWEP.AutoSwitchFrom = false
SWEP.Weight = 20
SWEP.Slot = 2
SWEP.SlotPos = 1

SWEP.UseHands = true
SWEP.HoldBase = "dods-mp40"
SWEP.HoldType = SWEP.HoldBase .. "-idle"
SWEP.FiresUnderwater = true
SWEP.DrawCrosshair = false
SWEP.DrawAmmo = true
SWEP.CSMuzzleFlashes = 1
SWEP.Base = "weapon_base"

SWEP.Primary.Sound = Sound( "Weapon_MP40.Shoot" )
SWEP.Primary.ClipSize = 30
SWEP.Primary.DefaultClip = 210
SWEP.Primary.MaxAmmo = 180
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "SMG1"
SWEP.Primary.Damage = 40
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.Spread = 0.055
SWEP.Primary.SpreadMovement = 0.155
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Delay = 0.09
SWEP.Primary.Force = 1

SWEP.Secondary.ClipSize = 0
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"
SWEP.Secondary.Damage = 60
SWEP.Secondary.Delay = 0.4
SWEP.Secondary.Force = 1000

function SWEP:InitializeNetVars()
    self:SetAttack( 0 )
    self:SetSprint( 0 )
    self:SetReloading( 0 )
    self:SetIdle( 0 )
    self:SetRecoil( 0 )
    self:SetRecoilDirection( 0 )

    self:SetAttackTimer( 0 )
    self:SetReloadingTimer( 0 )
    self:SetIdleTimer( 0 )
    self:SetRecoilTimer( 0 )

end

function SWEP:SetupDataTables()

    self.SprintAngles = 0

	self:NetworkVar( "Int", 0, "Attack" )
	self:NetworkVar( "Int", 1, "Sprint" )
	self:NetworkVar( "Int", 2, "Reloading" )
	self:NetworkVar( "Int", 3, "Idle" )
	self:NetworkVar( "Int", 4, "Recoil" )
    self:NetworkVar( "Int", 5, "Mode" )
    
	self:NetworkVar( "Float", 0, "AttackTimer" )
	self:NetworkVar( "Float", 1, "ReloadingTimer" )
	self:NetworkVar( "Float", 2, "IdleTimer" )
	self:NetworkVar( "Float", 3, "RecoilTimer" )
	self:NetworkVar( "Float", 4, "RecoilDirection" )
    
	if SERVER then
        self:InitializeNetVars()
        self:SetMode( 0 )
	end

end

function SWEP:Initialize()
    self:SetHoldType( self.HoldBase .. "-idle" )
    if SERVER then
        self:SetIdleTimer( CurTime() + 1 )
    end
end

function SWEP:Deploy()
    self:SetHoldType( self.HoldType )
    
    if self:GetMode() == 0 then
        self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
    else
        self.Weapon:SendWeaponAnim( ACT_VM_DRAW_DEPLOYED )
    end
    self:SetNextPrimaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
    self:SetNextSecondaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )

    if SERVER then
        self:InitializeNetVars()
        self:SetIdleTimer( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
        self:SetRecoilDirection( math.Rand( -0.1, 0.1 ) )
    end

    self.SprintAngles = 0

    return true
end

function SWEP:Holster()
    if SERVER then
        self:InitializeNetVars()
    end

    if IsValid( self.ThirdWeapon ) then
        self.ThirdWeapon:Remove()
    end

    self:ExitSightMode()
    
    return true
end

function SWEP:PrimaryRocketAttack()
    if self:GetMode() == 0 then return end
    if SERVER then
        local rc = ( self.BazookaRocket and "ent_bazooka_rocket" ) or "ent_pschreck_rocket"
        local entity = ents.Create( rc )
        entity:SetOwner( self.Owner )
        if IsValid( entity ) then
            local Forward = self.Owner:EyeAngles():Forward()
            local Right = self.Owner:EyeAngles():Right()
            local Up = self.Owner:EyeAngles():Up()
            entity:SetPos( self.Owner:GetShootPos() + Forward * 4 + Right * 4 + Up * 2 )
            entity:SetAngles( self.Owner:EyeAngles() )
            entity:Spawn()
            local phys = entity:GetPhysicsObject()
            if IsValid( phys ) then
                phys:Wake()
                phys:SetVelocity( self.Owner:GetAimVector() * self.Primary.Force )
            end
        end
    end
    self:EmitSound( self.Primary.Sound )
    self.Owner:MuzzleFlash()
    self.Recoil = 1
    self.RecoilTimer = CurTime() + 0.1
    self.RecoilDirection = math.Rand( -0.1, 0.1 )
    self:TakePrimaryAmmo( self.Primary.TakeAmmo )
    self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
    self.Owner:SetAnimation( PLAYER_ATTACK1 )
    self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    if SERVER then
        self:SetIdle( 0 )
        self:SetIdleTimer( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
    end
end

function SWEP:PrimaryLauncherAttack()
    if SERVER then
        local rc = ( self.KarGrenade and "ent_riflegren_ger" ) or "ent_riflegren_us"
        local entity = ents.Create( rc )
        entity:SetOwner( self.Owner )
        if IsValid( entity ) then
            local Forward = self.Owner:EyeAngles():Forward()
            local Right = self.Owner:EyeAngles():Right()
            local Up = self.Owner:EyeAngles():Up()
            entity:SetPos( self.Owner:GetShootPos() + Forward * 4 + Right * 4 + Up * 2 )
            entity:SetAngles( self.Owner:EyeAngles() )
            entity:Spawn()
            local phys = entity:GetPhysicsObject()
            if IsValid( phys ) then
                phys:Wake()
                phys:SetVelocity( self.Owner:GetAimVector() * self.Primary.Force )
            end
        end
    end
    self:EmitSound( self.Primary.Sound )
    self.Owner:MuzzleFlash()
    self.Recoil = 1
    self.RecoilTimer = CurTime() + 0.1
    self.RecoilDirection = math.Rand( -0.1, 0.1 )
    self:TakePrimaryAmmo( self.Primary.TakeAmmo )
    self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
    self.Owner:SetAnimation( PLAYER_ATTACK1 )
    self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    if SERVER then
        self:SetIdle( 0 )
        self:SetIdleTimer( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
    end
end

function SWEP:PrimaryDeployableAttack()
    if self:GetSprint() == 1 then return end
    if self:GetReloading() == 1 then return end

    if self.Weapon:Clip1() <= 0 and self.Weapon:Ammo1() <= 0 then
        self.Weapon:EmitSound( "Default.ClipEmpty_Rifle" )
        self:SetNextPrimaryFire( CurTime() + 0.2 )
    end

    if self.FiresUnderwater == false and self.Owner:WaterLevel() == 3 then
        self.Weapon:EmitSound( "Default.ClipEmpty_Rifle" )
        self:SetNextPrimaryFire( CurTime() + 0.2 )
        return
    end

    if self.Weapon:Clip1() <= 0 then
        self:Reload()
        return
    end

    local bullet = {}
    bullet.Num = self.Primary.NumberofShots
    bullet.Src = self.Owner:GetShootPos()
    bullet.Dir = self.Owner:GetAimVector()

    if self.Owner:GetVelocity():Length() < 100 then
        if self:GetMode() == 0 then
            bullet.Spread = Vector( 1 * self.Primary.Spread, 1 * self.Primary.Spread, 0 )
        end
        if self:GetMode() == 1 then
            bullet.Spread = Vector( 1 * self.Primary.SpreadSecondary, 1 * self.Primary.SpreadSecondary, 0 )
        end
    end

    if self.Owner:GetVelocity():Length() >= 100 then
        bullet.Spread = Vector( 1 * self.Primary.SpreadMovement, 1 * self.Primary.SpreadMovement, 0 )
    end

    if !self.Owner:IsOnGround() then
        bullet.Spread = Vector( 1 * self.Primary.SpreadMovement, 1 * self.Primary.SpreadMovement, 0 )
    end

    bullet.Tracer = 1
    bullet.Force = self.Primary.Force
    bullet.Damage = self.Primary.Damage
    bullet.AmmoType = self.Primary.Ammo
    self.Owner:FireBullets( bullet )
    self:EmitSound( self.Primary.Sound )
    self.Owner:MuzzleFlash()

    self:TakePrimaryAmmo( self.Primary.TakeAmmo )
    if self:GetMode() == 0 then
        if self.Weapon:Clip1() > 8 then
            self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
        end
        if self.Weapon:Clip1() == 8 then
            self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_8 )
        end
        if self.Weapon:Clip1() == 7 then
            self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_7 )
        end
        if self.Weapon:Clip1() == 6 then
            self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_6 )
        end
        if self.Weapon:Clip1() == 5 then
            self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_5 )
        end
        if self.Weapon:Clip1() == 4 then
            self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_4 )
        end
        if self.Weapon:Clip1() == 3 then
            self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_3 )
        end
        if self.Weapon:Clip1() == 2 then
            self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_2 )
        end
        if self.Weapon:Clip1() <= 1 then
            self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_1 )
        end
    elseif self:GetMode() == 1 then
        if self.Weapon:Clip1() > 8 then
            self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_DEPLOYED )
        end
        if self.Weapon:Clip1() == 8 then
            self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_DEPLOYED_8 )
        end
        if self.Weapon:Clip1() == 7 then
            self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_DEPLOYED_7 )
        end
        if self.Weapon:Clip1() == 6 then
            self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_DEPLOYED_6 )
        end
        if self.Weapon:Clip1() == 5 then
            self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_DEPLOYED_5 )
        end
        if self.Weapon:Clip1() == 4 then
            self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_DEPLOYED_4 )
        end
        if self.Weapon:Clip1() == 3 then
            self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_DEPLOYED_3 )
        end
        if self.Weapon:Clip1() == 2 then
            self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_DEPLOYED_2 )
        end
        if self.Weapon:Clip1() <= 1 then
            self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_DEPLOYED_1 )
        end
    end
    
    self.Owner:SetAnimation( PLAYER_ATTACK1 )
    self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    self:SetNextSecondaryFire( CurTime() + self.Primary.Delay )

    if self:GetMode() == 0 then
        self:SetHoldType( self.HoldBase .. "-shoot" )
    else
        self:SetHoldType( self.HoldBase .. "-deploy" )
    end

    if SERVER then
        if self:GetMode() == 0 then
            self:SetRecoil( 1 )
            self:SetRecoilTimer( CurTime() + 0.1 )
            self:SetRecoilDirection( math.Rand( -0.1, 0.1 ) )
        end
        self:SetIdle( 0 )
        self:SetIdleTimer( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
    end

end 

function SWEP:PrimaryAttack()
    if !self.Owner:OnGround() then return end
    if self:GetSprint() == 1 then return end
    if self:GetReloading() == 1 then return end
    if self.Weapon:Clip1() <= 0 and self.Weapon:Ammo1() <= 0 then
        self.Weapon:EmitSound( "Default.ClipEmpty_Rifle" )
        self:SetNextPrimaryFire( CurTime() + 0.2 )
    end
    if self.FiresUnderwater == false and self.Owner:WaterLevel() == 3 then
        self.Weapon:EmitSound( "Default.ClipEmpty_Rifle" )
        self:SetNextPrimaryFire( CurTime() + 0.2 )
    end
    if self.Weapon:Clip1() <= 0 then
        self:Reload()
    end
    if self.Weapon:Clip1() <= 0 then return end
    if self.FiresUnderwater == false and self.Owner:WaterLevel() == 3 then return end

    if self.KarGrenade or self.GarGrenade then
        self:PrimaryLauncherAttack()
        return
    end

    if self.PanzerRocket or self.BazookaRocket then
        self:PrimaryRocketAttack()
        return
    end

    if self.Deployable then
        self:PrimaryDeployableAttack()
        return
    end

    if self.MeleeWeapon then
        self:PrimaryMeleeAttack()
        return
    end

    local bullet = {}
    bullet.Num = self.Primary.NumberofShots
    bullet.Src = self.Owner:GetShootPos()
    bullet.Dir = self.Owner:GetAimVector()
    if self:GetMode() == 0 then
        if self.Owner:GetVelocity():Length() < 100 then
            bullet.Spread = Vector( 1 * self.Primary.Spread, 1 * self.Primary.Spread, 0 )
        end
        if self.Owner:GetVelocity():Length() >= 100 then
            bullet.Spread = Vector( 1 * self.Primary.SpreadMovement, 1 * self.Primary.SpreadMovement, 0 )
        end
        if !self.Owner:IsOnGround() then
            bullet.Spread = Vector( 1 * self.Primary.SpreadMovement, 1 * self.Primary.SpreadMovement, 0 )
        end
    elseif self:GetMode() == 1 then
        bullet.Spread = Vector( 1 * self.Primary.SpreadSecondary, 1 * self.Primary.SpreadSecondary, 0 )
    end

    bullet.Tracer = 1
    bullet.Force = self.Primary.Force
    bullet.Damage = self.Primary.Damage
    bullet.AmmoType = self.Primary.Ammo
    self.Owner:FireBullets( bullet )
    self:EmitSound( self.Primary.Sound )
    self.Owner:MuzzleFlash()
    self:TakePrimaryAmmo( self.Primary.TakeAmmo )

    if self:Clip1() < 1 and self.GarandReload then
        self:EmitSound( "weapons/garand_clipding.wav" )
    end

    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self:SetNextSecondaryFire( CurTime() + self.Primary.Delay )

    if self:GetMode() == 1 and ( self.SpringScope or self.KarScope or self.IronSights ) then
        self:ExitSightMode()
    end

    if self:GetMode() == 0 then
        self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
    elseif self:GetMode() == 1 and self.SingleSecondary then
        self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_DEPLOYED )
        self:SetNextPrimaryFire( CurTime() + self.Primary.DelaySecondary )
        self:SetNextSecondaryFire( CurTime() + self.Primary.DelaySecondary )
    end

    self:SetHoldType( self.HoldBase .. "-shoot" )
    if SERVER then
        self:SetRecoil( 1 )
        self:SetRecoilTimer( CurTime() + 0.1 )
        self:SetRecoilDirection( math.Rand( -0.1, 0.1 ) )
        self:SetIdle( 0 )
        self:SetIdleTimer( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
    end

end

function SWEP:SecondaryDeployableAttack()
    if !self.Owner:IsOnGround() then return end
    if self:GetSprint() == 1 then return end
    if self:GetReloading() == 1 then return end
    if self:GetMode() == 0 then
        if self.Weapon:Clip1() > 8 then
            self.Weapon:SendWeaponAnim( ACT_VM_DEPLOY )
        end
        if self.Weapon:Clip1() == 8 then
            self.Weapon:SendWeaponAnim( ACT_VM_DEPLOY_8 )
        end
        if self.Weapon:Clip1() == 7 then
            self.Weapon:SendWeaponAnim( ACT_VM_DEPLOY_7 )
        end
        if self.Weapon:Clip1() == 6 then
            self.Weapon:SendWeaponAnim( ACT_VM_DEPLOY_6 )
        end
        if self.Weapon:Clip1() == 5 then
            self.Weapon:SendWeaponAnim( ACT_VM_DEPLOY_5 )
        end
        if self.Weapon:Clip1() == 4 then
            self.Weapon:SendWeaponAnim( ACT_VM_DEPLOY_4 )
        end
        if self.Weapon:Clip1() == 3 then
            self.Weapon:SendWeaponAnim( ACT_VM_DEPLOY_3 )
        end
        if self.Weapon:Clip1() == 2 then
            self.Weapon:SendWeaponAnim( ACT_VM_DEPLOY_2 )
        end
        if self.Weapon:Clip1() <= 1 then
            self.Weapon:SendWeaponAnim( ACT_VM_DEPLOY_EMPTY )
        end
        self:SetNextPrimaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
        self:SetNextSecondaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
        self.CrosshairAlpha = 255
        if SERVER then
            self:SetMode( 1 )
            self:SetIdle( 0 )
            self:SetIdleTimer( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
            self.Owner:SetWalkSpeed( 1 )
            self.Owner:SetRunSpeed( 1 )
            self.Owner:SetJumpPower( 0 )
        end
    elseif self:GetMode() == 1 then
        if self.Weapon:Clip1() > 8 then
            self.Weapon:SendWeaponAnim( ACT_VM_UNDEPLOY )
        end
        if self.Weapon:Clip1() == 8 then
            self.Weapon:SendWeaponAnim( ACT_VM_UNDEPLOY_8 )
        end
        if self.Weapon:Clip1() == 7 then
            self.Weapon:SendWeaponAnim( ACT_VM_UNDEPLOY_7 )
        end
        if self.Weapon:Clip1() == 6 then
            self.Weapon:SendWeaponAnim( ACT_VM_UNDEPLOY_6 )
        end
        if self.Weapon:Clip1() == 5 then
            self.Weapon:SendWeaponAnim( ACT_VM_UNDEPLOY_5 )
        end
        if self.Weapon:Clip1() == 4 then
            self.Weapon:SendWeaponAnim( ACT_VM_UNDEPLOY_4 )
        end
        if self.Weapon:Clip1() == 3 then
            self.Weapon:SendWeaponAnim( ACT_VM_UNDEPLOY_3 )
        end
        if self.Weapon:Clip1() == 2 then
            self.Weapon:SendWeaponAnim( ACT_VM_UNDEPLOY_2 )
        end
        if self.Weapon:Clip1() <= 1 then
            self.Weapon:SendWeaponAnim( ACT_VM_UNDEPLOY_EMPTY )
        end
        self:SetNextPrimaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
        self:SetNextSecondaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
        self.CrosshairAlpha = 0
        if SERVER then
            self:SetMode( 0 )
            self:SetIdle( 0 )
            self:SetIdleTimer( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
            self.Owner:SetWalkSpeed( 200 )
            self.Owner:SetRunSpeed( 400 )
            self.Owner:SetJumpPower( 200 )
        end
    end
end

function SWEP:SecondaryAttack()
    if self.Owner:KeyDown( IN_SPEED ) then return end
    if !self.Owner:OnGround() then return end
    if self:GetReloading() == 1 then return end

    if self.MeleeSecondary then
        self.Weapon:SendWeaponAnim( ACT_VM_SECONDARYATTACK )
        self.Owner:DoCustomAnimEvent( PLAYERANIMEVENT_ATTACK_SECONDARY, 0 )
        self:SetNextPrimaryFire( CurTime() + self.Secondary.Delay )
        self:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

        self:SetHoldType( self.HoldBase .. "-shoot" )

        if SERVER then
            self:SetAttack( 1 )
            self:SetAttackTimer( CurTime() + 0.2 )
            self:SetIdle( 0 )
            self:SetIdleTimer( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
        end
    elseif self.SingleSecondary then
        if self:GetMode() == 0 then
            self.Weapon:SendWeaponAnim( ACT_VM_DEPLOY )
            self:SetNextPrimaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
            self:SetNextSecondaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )

            self.FireModeScale = 0.1
            if SERVER then
                self:SetMode( 1 )
                self:SetIdle( 0 )
                self:SetIdleTimer( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
            end
        elseif self:GetMode() == 1 then
            self.Weapon:SendWeaponAnim( ACT_VM_UNDEPLOY )
            self:SetNextPrimaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
            self:SetNextSecondaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
            self.FireModeScale = -0.1
            if SERVER then
                self:SetMode( 0 )
                self:SetIdle( 0 )
                self:SetIdleTimer( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
            end
        end
    elseif self.IronSights then
        self:SetNextPrimaryFire( CurTime() + self.Secondary.Delay )
        self:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
        if self:GetMode() == 0 then
            -- self.AimTimer = CurTime() + 0.3
            -- self.AimTimerFOV = 1
            -- self.Weapon:SetNWString( "AimScale", 0.1 )
            -- self.Weapon:SetNWString( "CrosshairAlpha", 0 )
            self.FireModeScale = 0.1
            if SERVER then
                self.Owner:SetFOV( 50, 0.1 )
                self:SetMode( 1 )
            end
        elseif self:GetMode() == 1 then
            -- self.Aim = 0
            -- self.AimTimer = CurTime()
            -- self.AimTimerFOV = 0
            -- self.Weapon:SetNWString( "BobSway", 1 )
            -- self.Weapon:SetNWString( "AimScale", -0.1 )
            -- self.Weapon:SetNWString( "CrosshairAlpha", 255 )
            -- self.Weapon:SetNWString( "MouseSensitivity", 1 )
            self.FireModeScale = -0.1
            self:ExitSightMode()
        end
    elseif self.SpringScope or self.KarScope then
        self:SetNextPrimaryFire( CurTime() + self.Secondary.Delay )
        self:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
        if self:GetMode() == 0 then
            -- self.AimTimer = CurTime() + 0.3
            -- self.AimTimerFOV = 1
            -- self.Weapon:SetNWString( "AimScale", 0.1 )
            -- self.Weapon:SetNWString( "CrosshairAlpha", 0 )
            self.FireModeScale = 0.1
            if SERVER then
                self.Owner:SetFOV( 20, 0.1 )
                self:SetMode( 1 )
            end
        elseif self:GetMode() == 1 then
            -- self.Aim = 0
            -- self.AimTimer = CurTime()
            -- self.AimTimerFOV = 0
            -- self.Weapon:SetNWString( "BobSway", 1 )
            -- self.Weapon:SetNWString( "AimScale", -0.1 )
            -- self.Weapon:SetNWString( "CrosshairAlpha", 255 )
            -- self.Weapon:SetNWString( "MouseSensitivity", 1 )
            self.FireModeScale = -0.1
            self:ExitSightMode()
        end
    elseif self.PanzerRocket or self.BazookaRocket then
        if self:GetMode() == 0 then
            self.Weapon:SendWeaponAnim( ACT_VM_DEPLOY )
            self:SetNextPrimaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
            self:SetNextSecondaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
            if SERVER then
                self:SetMode( 1 )
                self:SetIdle( 0 )
                self.Owner:SetWalkSpeed( 50 )
                self:SetIdleTimer( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
            end
        elseif self:GetMode() == 1 then
            self.Weapon:SendWeaponAnim( ACT_VM_UNDEPLOY )
            self:SetNextPrimaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
            self:SetNextSecondaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
            if SERVER then
                self:SetMode( 0 )
                self:SetIdle( 0 )
                self.Owner:SetWalkSpeed( 200 )
                self:SetIdleTimer( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
            end
        end
    elseif self.Deployable then
        self:SecondaryDeployableAttack()
    end

end

function SWEP:ExitSightMode()

    if SERVER then
        self.Owner:SetFOV( 0, 0.1 )
        self:SetIdleTimer( 0 )
        self:SetMode( 0 )
        self.Owner:SetWalkSpeed( 200 )
        self.Owner:SetRunSpeed( 400 )
        self.Owner:SetJumpPower( 200 )
    end
    self.ScopeAlpha = 0
    self.FireModeScale = -0.1

end

function SWEP:Reload()
    if self.GarandReload and self.Weapon:Clip1() != 0 then return end
    if self.Deployable and self:GetMode() != 1 then return end
    if self:GetReloading() == 0 and self.Weapon:Clip1() < self.Primary.ClipSize and self.Weapon:Ammo1() > 0 then

        if self:GetMode() == 0 or self.MG42Deployable then
            self.Weapon:SendWeaponAnim( ACT_VM_RELOAD )
        else
            self.Weapon:SendWeaponAnim( ACT_VM_RELOAD_DEPLOYED )
        end

        self.Owner:SetAnimation( PLAYER_RELOAD )
        self:SetNextPrimaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
        self:SetNextSecondaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
        self:SetHoldType( self.HoldBase .. "-shoot" )

        if SERVER then
            self:SetAttack( 0 )
            self:SetAttackTimer( CurTime() )
            self:SetReloading( 1 )
            self:SetReloadingTimer( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
            self:SetIdle( 0 )
            self:SetIdleTimer( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
        end

    end
end

function SWEP:MeleeStrike()
    if self:GetAttack() == 1 and self:GetAttackTimer() <= CurTime() then
        local tr = util.TraceLine( {
            start = self.Owner:GetShootPos(),
            endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * 72,
            filter = self.Owner,
            mask = MASK_SHOT_HULL,
        } )
        if !IsValid( tr.Entity ) then
            tr = util.TraceHull( {
                start = self.Owner:GetShootPos(),
                endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * 72,
                filter = self.Owner,
                mins = Vector( -16, -16, 0 ),
                maxs = Vector( 16, 16, 0 ),
                mask = MASK_SHOT_HULL,
            } )
        end
        if SERVER then
            if tr.Hit and !( tr.Entity:IsNPC() || tr.Entity:IsPlayer() ) then
                self.Owner:EmitSound( "Weapon_Punch.HitWorld" )
            end
            if IsValid( tr.Entity ) then
                local dmginfo = DamageInfo()
                local attacker = self.Owner
                if !IsValid( attacker ) then
                    attacker = self
                end
                dmginfo:SetAttacker( attacker )
                dmginfo:SetInflictor( self )
                dmginfo:SetDamage( self.Secondary.Damage )
                dmginfo:SetDamageForce( self.Owner:GetForward() * self.Secondary.Force )
                tr.Entity:TakeDamageInfo( dmginfo )
                if tr.Hit then
                    if tr.Entity:IsNPC() || tr.Entity:IsPlayer() then
                        self.Owner:EmitSound( "Weapon_Punch.HitPlayer" )
                    end
                    if !( tr.Entity:IsNPC() || tr.Entity:IsPlayer() ) then
                        self.Owner:EmitSound( "Weapon_Punch.HitWorld" )
                    end
                end
            end
        end
        self:SetAttack( 0 )
    end
end

function SWEP:FireModeToggle()
    if self:GetMode() == 0 then
        self.Primary.Automatic = true
    elseif self:GetMode() == 1 then
        self.Primary.Automatic = false
    end

    self.FireModeForward = math.Clamp( self.FireModeForward - self.FireModeScale * self.FireForwardScale, self.FireForwardMin, self.FireForwardMax )

    self.FireModeRight = math.Clamp( self.FireModeRight - self.FireModeScale * self.FireRightScale, self.FireRightMin, self.FireRightMax )

    self.FireModeUp = math.Clamp( self.FireModeUp + self.FireModeScale * self.FireUpScale, self.FireUpMin, self.FireUpMax )

end

function SWEP:IronSightToggle()
    if self:GetMode() == 1 then
        self:SetHoldType( self.HoldBase .. "-shoot" )
        if SERVER then
            self:SetIdleTimer( CurTime() + 1 )
            self:SetIdle( 0 )
            self.Owner:SetWalkSpeed( 50 )
        end
        if self.Owner:KeyDown( IN_SPEED ) or not self.Owner:OnGround() then
            self:ExitSightMode()
        end
    end

    self.AimForward = math.Clamp( self.AimForward - self.FireModeScale * self.FireForwardScale, self.FireForwardMin, self.FireForwardMax )

    self.AimRight = math.Clamp( self.AimRight - self.FireModeScale * self.FireRightScale, self.FireRightMin, self.FireRightMax )

    self.AimUp = math.Clamp( self.AimUp + self.FireModeScale * self.FireUpScale, self.FireUpMin, self.FireUpMax )

end


function SWEP:ScopeToggle()
    if self:GetMode() == 1 then
        self:SetHoldType( self.HoldBase .. "-shoot" )
        self.ScopeAlpha = 255
        if SERVER then
            self:SetIdleTimer( CurTime() + 1 )
            self:SetIdle( 0 )
            self.Owner:SetWalkSpeed( 50 )
        end
        if self.Owner:KeyDown( IN_SPEED ) or not self.Owner:OnGround() then
            self:ExitSightMode()
        end
    else
        self.ScopeAlpha = 0
    end

    self.AimForward = math.Clamp( self.AimForward - self.FireModeScale * self.FireForwardScale, self.FireForwardMin, self.FireForwardMax )

    self.AimRight = math.Clamp( self.AimRight - self.FireModeScale * self.FireRightScale, self.FireRightMin, self.FireRightMax )

    self.AimUp = math.Clamp( self.AimUp + self.FireModeScale * self.FireUpScale, self.FireUpMin, self.FireUpMax )

end

function SWEP:RocketThoughts()
    if self:GetSprint() == 0 and self.Owner:KeyDown( IN_SPEED ) and ( self.Owner:KeyDown( IN_FORWARD ) || self.Owner:KeyDown( IN_BACK ) || self.Owner:KeyDown( IN_MOVELEFT ) || self.Owner:KeyDown( IN_MOVERIGHT ) ) then
        if self:GetMode() == 1 then
            if self:GetReloading() == 0 then
                self.Weapon:SendWeaponAnim( ACT_VM_UNDEPLOY )
                self:SetNextPrimaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
                self:SetNextSecondaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
                if SERVER then
                    self:SetIdle( 0 )
                    self:SetIdleTimer( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
                end
            end
            if SERVER then
                self:SetMode( 0 )
                self.Owner:SetWalkSpeed( 200 )
            end
        end
    end

    if self:GetMode() == 1 then
        self:SetHoldType( self.HoldBase .. "-shoot" )
        self:SetIdleTimer( CurTime() + 1 )
        self:SetIdle( 0 )
    end

end

function SWEP:DeployableThoughts()
    if self:GetMode() == 1 and self:GetReloading() == 0 and !self.Owner:IsOnGround() then
        if self.Weapon:Clip1() > 8 then
            self.Weapon:SendWeaponAnim( ACT_VM_UNDEPLOY )
        end
        if self.Weapon:Clip1() == 8 then
            self.Weapon:SendWeaponAnim( ACT_VM_UNDEPLOY_8 )
        end
        if self.Weapon:Clip1() == 7 then
            self.Weapon:SendWeaponAnim( ACT_VM_UNDEPLOY_7 )
        end
        if self.Weapon:Clip1() == 6 then
            self.Weapon:SendWeaponAnim( ACT_VM_UNDEPLOY_6 )
        end
        if self.Weapon:Clip1() == 5 then
            self.Weapon:SendWeaponAnim( ACT_VM_UNDEPLOY_5 )
        end
        if self.Weapon:Clip1() == 4 then
            self.Weapon:SendWeaponAnim( ACT_VM_UNDEPLOY_4 )
        end
        if self.Weapon:Clip1() == 3 then
            self.Weapon:SendWeaponAnim( ACT_VM_UNDEPLOY_3 )
        end
        if self.Weapon:Clip1() == 2 then
            self.Weapon:SendWeaponAnim( ACT_VM_UNDEPLOY_2 )
        end
        if self.Weapon:Clip1() <= 1 then
            self.Weapon:SendWeaponAnim( ACT_VM_UNDEPLOY_EMPTY )
        end
        self:SetNextPrimaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
        self:SetNextSecondaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
        self:ExitSightMode()
    end

    if self:GetMode() == 1 then
        self:SetHoldType( self.HoldBase .. "-deploy" )
        self:SetIdleTimer( CurTime() + 1 )
        self:SetIdle( 0 )
        self.CrosshairAlpha = 255
    else
        self.CrosshairAlpha = 0
    end

end

function SWEP:Think()

    if self.MeleeSecondary then
        self:MeleeStrike()
    elseif self.SingleSecondary then
        self:FireModeToggle()
    elseif self.IronSights then
        self:IronSightToggle()
    elseif self.SpringScope or self.KarScope then
        self:ScopeToggle()
    elseif self.PanzerRocket or self.BazookaRocket then
        self:RocketThoughts()
    elseif self.Deployable then
        self:DeployableThoughts()
    end

    if SERVER then
        if self.Owner:GetVelocity():Length() < 100 then
            self.Weapon:SetNWInt( "Crosshair1", 16 )
            self.Weapon:SetNWInt( "Crosshair2", 8 )
        end

        if self.Owner:GetVelocity():Length() >= 100 then
            self.Weapon:SetNWInt( "Crosshair1", 22 )
            self.Weapon:SetNWInt( "Crosshair2", 14 )
        end

        if !self.Owner:IsOnGround() then
            self.Weapon:SetNWInt( "Crosshair1", 22 )
            self.Weapon:SetNWInt( "Crosshair2", 14 )
        end
    end

    if self:GetSprint() == 0 and self.SprintAngles < 0 then
        self.SprintAngles = self.SprintAngles + 3
    end

    if self:GetSprint() == 1 and self.SprintAngles > -30 then
        self.SprintAngles = self.SprintAngles - 3
    end

    if self:GetRecoil() == 1 then
        self.Owner:SetEyeAngles( self.Owner:EyeAngles() + Angle( -0.33, self:GetRecoilDirection(), 0 ) )
    end

    if self:GetSprint() == 0 and self.Owner:KeyDown( IN_SPEED ) and ( self.Owner:KeyDown( IN_FORWARD ) || self.Owner:KeyDown( IN_BACK ) || self.Owner:KeyDown( IN_MOVELEFT ) || self.Owner:KeyDown( IN_MOVERIGHT ) ) then
        if SERVER then
            self:SetSprint( 1 )
        end
        self.SprintAngles = 0
    end

    if SERVER then

        if self:GetRecoil() == 1 and self:GetRecoilTimer() <= CurTime() then
            self:SetRecoil( 0 )
        end    

        if self:GetSprint() == 1 and !self.Owner:KeyDown( IN_SPEED ) then
            self:SetSprint( 0 )
        end

        if self:GetSprint() == 1 and !( self.Owner:KeyDown( IN_FORWARD ) || self.Owner:KeyDown( IN_BACK ) || self.Owner:KeyDown( IN_MOVELEFT ) || self.Owner:KeyDown( IN_MOVERIGHT ) ) then
            self:SetSprint( 0 )
        end

        if self:GetReloading() == 1 and self:GetReloadingTimer() <= CurTime() then
            if self.Weapon:Ammo1() > ( self.Primary.ClipSize - self.Weapon:Clip1() ) then
                self.Owner:SetAmmo( self.Weapon:Ammo1() - self.Primary.ClipSize + self.Weapon:Clip1(), self.Primary.Ammo )
                self.Weapon:SetClip1( self.Primary.ClipSize )
            end
            if ( self.Weapon:Ammo1() - self.Primary.ClipSize + self.Weapon:Clip1() ) + self.Weapon:Clip1() < self.Primary.ClipSize then
                self.Weapon:SetClip1( self.Weapon:Clip1() + self.Weapon:Ammo1() )
                self.Owner:SetAmmo( 0, self.Primary.Ammo )
            end
            self:SetReloading( 0 )
        end

    end

    if self:GetIdle() == 0 and self:GetIdleTimer() < CurTime() then
        if SERVER then
            if self:GetMode() == 0 then
                self.Weapon:SendWeaponAnim( ACT_VM_IDLE )
            elseif self:GetMode() == 1 then
                self.Weapon:SendWeaponAnim( ACT_VM_IDLE_DEPLOYED )
            end
            self:SetIdle( 1 )
        end
        self:SetHoldType( self.HoldBase .. "-idle" )
    end

    if self.Weapon:Ammo1() > self.Primary.MaxAmmo then
        self.Owner:SetAmmo( self.Primary.MaxAmmo, self.Primary.Ammo )
    end

end

if SERVER then return end

function SWEP:GetViewModelPosition( pos, ang )
    //pos = pos + ang:Forward() * -1 + ang:Right() * 0.6 + ang:Up() * 0
    if self.SingleSecondary then
        pos = pos + ang:Forward() * self.FireModeForward + ang:Right() * self.FireModeRight + ang:Up() * self.FireModeUp
    elseif self.IronSights then
        pos = pos + ang:Forward() * self.AimForward + ang:Right() * self.AimRight + ang:Up() * self.AimUp
    end
    ang:RotateAroundAxis( ang:Right(), self.SprintAngles )
    return pos, ang
end

local spl, spr, spul, spur, spc = surface.GetTextureID( "sprites/scopes/scope_spring_ll" ), surface.GetTextureID( "sprites/scopes/scope_spring_lr" ), surface.GetTextureID( "sprites/scopes/scope_spring_ul" ), surface.GetTextureID( "sprites/scopes/scope_spring_ur" ), surface.GetTextureID( "sprites/scopes/black" )
local kpl, kpr, kpul, kpur = surface.GetTextureID( "sprites/scopes/scope_k43_ll" ), surface.GetTextureID( "sprites/scopes/scope_k43_lr" ), surface.GetTextureID( "sprites/scopes/scope_k43_ul" ), surface.GetTextureID( "sprites/scopes/scope_k43_ur" )

function SWEP:DrawHUD()
    if self:GetMode() == 1 then
        local x = ScrW() / 2
        local y = ScrH() / 2
        if self.SpringScope then
            surface.SetDrawColor( 255, 255, 255, self.ScopeAlpha )
            surface.SetTexture( spl )
            surface.DrawTexturedRect( x - ScrH() / 1.5, y - 0, ScrH() / 1.5, ScrH() / 2 )
            surface.SetTexture( spr )
            surface.DrawTexturedRect( x - 0, y - 0, ScrH() / 1.5, ScrH() / 2 )
            surface.SetTexture( spul )
            surface.DrawTexturedRect( x - ScrH() / 1.5, y - ScrH() / 2, ScrH() / 1.5, ScrH() / 2 )
            surface.SetTexture( spur )
            surface.DrawTexturedRect( x - 0, y - ScrH() / 2, ScrH() / 1.5, ScrH() / 2 )
            surface.SetTexture( spc )
            surface.DrawTexturedRect( x - ScrW() / 2, y - ScrH() / 2, ScrW() / 2 - ScrH() / 2, ScrH() )
            surface.SetTexture( spc )
            surface.DrawTexturedRect( x - -ScrH() / 2, y - ScrH() / 2, ScrW() / 2 - ScrH() / 2, ScrH() )
            surface.SetDrawColor( 0, 0, 0, self.ScopeAlpha )
            surface.DrawLine( x - ScrH(), y, x + ScrH(), y )
            surface.DrawLine( x, y - ScrH(), x, y + ScrH() )
        elseif self.KarScope then
            surface.SetDrawColor( 255, 255, 255, self.ScopeAlpha )
            surface.SetTexture( kpl )
            surface.DrawTexturedRect( x - ScrH() / 1.5, y - 0, ScrH() / 1.5, ScrH() / 2 )
            surface.SetTexture( kpr )
            surface.DrawTexturedRect( x - 0, y - 0, ScrH() / 1.5, ScrH() / 2 )
            surface.SetTexture( kpul )
            surface.DrawTexturedRect( x - ScrH() / 1.5, y - ScrH() / 2, ScrH() / 1.5, ScrH() / 2 )
            surface.SetTexture( kpur )
            surface.DrawTexturedRect( x - 0, y - ScrH() / 2, ScrH() / 1.5, ScrH() / 2 )
            surface.SetTexture( spc )
            surface.DrawTexturedRect( x - ScrW() / 2, y - ScrH() / 2, ScrW() / 2 - ScrH() / 2, ScrH() )
            surface.SetTexture( spc )
            surface.DrawTexturedRect( x - -ScrH() / 2, y - ScrH() / 2, ScrW() / 2 - ScrH() / 2, ScrH() )
        end

    end

    if self.BlockDODCrosshair then return end
    local x = ScrW() / 2
    local y = ScrH() / 2
    surface.SetDrawColor( 255, 0, 0, 255 )
    surface.DrawLine( x - self.Weapon:GetNWInt( "Crosshair1", 16 ), y, x - self.Weapon:GetNWInt( "Crosshair2", 8 ), y )
    surface.DrawLine( x + self.Weapon:GetNWInt( "Crosshair1", 16 ), y, x + self.Weapon:GetNWInt( "Crosshair2", 8 ), y )
    surface.DrawLine( x, y - self.Weapon:GetNWInt( "Crosshair1", 16 ), x, y - self.Weapon:GetNWInt( "Crosshair2", 8 ) )
    surface.DrawLine( x, y + self.Weapon:GetNWInt( "Crosshair1", 16 ), x, y + self.Weapon:GetNWInt( "Crosshair2", 8 ) )
end