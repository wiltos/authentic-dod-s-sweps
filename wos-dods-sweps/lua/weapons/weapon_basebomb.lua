if CLIENT then
SWEP.WepSelectIcon = surface.GetTextureID( "sprites/hud/dods_basebomb" )
SWEP.DrawWeaponInfoBox	= false
SWEP.BounceWeaponIcon = false
killicon.Add( "weapon_basebomb", "sprites/hud/dods_basebomb", Color( 255, 255, 255, 255 ) )
killicon.Add( "ent_basebomb", "sprites/hud/dods_basebomb", Color( 255, 255, 255, 255 ) )
end

SWEP.PrintName = "Bomb"
SWEP.Category = "Day of Defeat: Source"
SWEP.Spawnable= true
SWEP.AdminSpawnable= true
SWEP.AdminOnly = false

SWEP.ViewModelFOV = 45
SWEP.ViewModel = "models/weapons/c_tnt.mdl"
SWEP.WorldModel = "models/weapons/p_tnt.mdl"
SWEP.ViewModelFlip = false

SWEP.AutoSwitchTo = false
SWEP.AutoSwitchFrom = false
SWEP.Weight = 3
SWEP.Slot = 5
SWEP.SlotPos = 1

SWEP.UseHands = true
SWEP.HoldType = "slam"
SWEP.FiresUnderwater = true
SWEP.DrawCrosshair = false
SWEP.DrawAmmo = true
SWEP.CSMuzzleFlashes = 1
SWEP.Base = "weapon_base"

SWEP.Plant = 0
SWEP.PlantTimer = CurTime()
SWEP.Sprint = 0
SWEP.SprintAngles = 0
SWEP.Idle = 0
SWEP.IdleTimer = CurTime()

SWEP.Primary.Sound = Sound( "Weapon_C4.Plant" )
SWEP.Primary.ClipSize = 0
SWEP.Primary.DefaultClip = 0
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "none"
SWEP.Primary.Delay = 2

SWEP.Secondary.ClipSize = 0
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

function SWEP:Initialize()
self:SetHoldType( self.HoldType )
self.Idle = 0
self.IdleTimer = CurTime() + 1
end

if CLIENT then
function SWEP:GetViewModelPosition( pos, ang )
pos = pos + ang:Forward() * 1 + ang:Right() * 1.5 + ang:Up() * 1
ang:RotateAroundAxis( ang:Right(), self.Weapon:GetNWString( "SprintAngles", self.SprintAngles ) )
return pos, ang
end
end

function SWEP:Deploy()
self:SetHoldType( self.HoldType )
self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
self:SetNextPrimaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
self:SetNextSecondaryFire( CurTime() + self.Owner:GetViewModel():SequenceDuration() )
self.Plant = 0
self.PlantTimer = CurTime()
self.Sprint = 0
self.SprintAngles = 0
self.Idle = 0
self.IdleTimer = CurTime() + self.Owner:GetViewModel():SequenceDuration()
self.Owner:SetWalkSpeed( 200 )
self.Owner:SetRunSpeed( 400 )
self.Owner:SetJumpPower( 200 )
return true
end

function SWEP:Holster()
if !( self.Plant == 2 ) then
self.Plant = 0
self.PlantTimer = CurTime()
end
self.Sprint = 0
self.SprintAngles = 0
self.Idle = 0
self.IdleTimer = CurTime()
self.Owner:SetWalkSpeed( 200 )
self.Owner:SetRunSpeed( 400 )
self.Owner:SetJumpPower( 200 )
if self.Plant == 2 then
self.Weapon:Remove()
end
return true
end

function SWEP:PrimaryAttack()
if !self.Owner:IsOnGround() then return end
if !( self.Plant == 0 ) then return end
if self.Sprint == 1 then return end
self:EmitSound( self.Primary.Sound )
self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
self:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
self.Plant = 1
self.PlantTimer = CurTime() + self.Primary.Delay
self.Idle = 1
self.IdleTimer = CurTime()
self.Owner:SetWalkSpeed( 1 )
self.Owner:SetRunSpeed( 1 )
self.Owner:SetJumpPower( 0 )
end

function SWEP:SecondaryAttack()
end

function SWEP:Reload()
end

function SWEP:Think()
if self.Plant == 1 and self.PlantTimer <= CurTime() then
if SERVER then
local entity = ents.Create( "ent_basebomb" )
entity:SetOwner( self.Owner )
if IsValid( entity ) then
local Forward = self.Owner:EyeAngles():Forward()
local Right = self.Owner:EyeAngles():Right()
local Up = self.Owner:EyeAngles():Up()
entity:SetPos( self.Owner:GetShootPos() + Forward * 16 + Right * 0 + Up * 0 )
entity:SetAngles( self.Owner:EyeAngles() + Angle( -90, 0, 0 ) )
entity:Spawn()
local phys = entity:GetPhysicsObject()
phys:SetVelocity( self.Owner:GetAimVector() * 0 )
end
end
self.Owner:SetAnimation( PLAYER_ATTACK1 )
self.Plant = 2
self.Owner:SetWalkSpeed( 200 )
self.Owner:SetRunSpeed( 400 )
self.Owner:SetJumpPower( 200 )
end
if self.Sprint == 0 and self.SprintAngles < 0 then
self.SprintAngles = self.SprintAngles + 3
end
if self.Sprint == 1 and self.SprintAngles > -30 then
self.SprintAngles = self.SprintAngles - 3
end
self.Weapon:SetNWString( "SprintAngles", self.SprintAngles )
if self.Sprint == 0 and self.Owner:KeyDown( IN_SPEED ) and ( self.Owner:KeyDown( IN_FORWARD ) || self.Owner:KeyDown( IN_BACK ) || self.Owner:KeyDown( IN_MOVELEFT ) || self.Owner:KeyDown( IN_MOVERIGHT ) ) then
self.Sprint = 1
self.SprintAngles = 0
end
if self.Sprint == 1 and !self.Owner:KeyDown( IN_SPEED ) then
self.Sprint = 0
end
if self.Sprint == 1 and !( self.Owner:KeyDown( IN_FORWARD ) || self.Owner:KeyDown( IN_BACK ) || self.Owner:KeyDown( IN_MOVELEFT ) || self.Owner:KeyDown( IN_MOVERIGHT ) ) then
self.Sprint = 0
end
if self.Idle == 0 and self.IdleTimer < CurTime() then
if SERVER then
self.Weapon:SendWeaponAnim( ACT_VM_IDLE )
end
self.Idle = 1
end
if self.Plant == 2 then
if SERVER then
self.Owner:DropWeapon( self.Weapon )
end
self.Weapon:Remove()
end
end