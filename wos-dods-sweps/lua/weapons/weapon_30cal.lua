if CLIENT then
SWEP.WepSelectIcon = surface.GetTextureID( "sprites/hud/dods_30cal" )
SWEP.DrawWeaponInfoBox	= false
SWEP.BounceWeaponIcon = false
killicon.Add( "weapon_30cal", "sprites/hud/dods_30cal", Color( 255, 255, 255, 255 ) )
end

SWEP.PrintName = ".30 Cal"
SWEP.Category = "Day of Defeat: Source"
SWEP.Spawnable= true
SWEP.AdminSpawnable= true
SWEP.AdminOnly = false

SWEP.ViewModelFOV = 45
SWEP.ViewModel = "models/weapons/c_30cal.mdl"
SWEP.WorldModel = "models/weapons/w_30cal.mdl"
SWEP.ViewModelFlip = false

SWEP.AutoSwitchTo = false
SWEP.AutoSwitchFrom = false
SWEP.Weight = 20
SWEP.Slot = 2
SWEP.SlotPos = 1

SWEP.UseHands = true
SWEP.HoldType = "ar2"
SWEP.FiresUnderwater = true
SWEP.DrawCrosshair = false
SWEP.DrawAmmo = true
SWEP.CSMuzzleFlashes = 1
SWEP.Base = "weapon_base"

SWEP.Bipods = 0
SWEP.Sprint = 0
SWEP.SprintAngles = 0
SWEP.Reloading = 0
SWEP.ReloadingTimer = CurTime()
SWEP.Idle = 0
SWEP.IdleTimer = CurTime()
SWEP.Recoil = 0
SWEP.RecoilTimer = CurTime()
SWEP.RecoilDirection = 0

SWEP.Primary.Sound = Sound( "Weapon_30cal.Shoot" )
SWEP.Primary.ClipSize = 150
SWEP.Primary.DefaultClip = 450
SWEP.Primary.MaxAmmo = 300
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "AR2"
SWEP.Primary.Damage = 85
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.Spread = 0.2
SWEP.Primary.SpreadSecondary = 0.01
SWEP.Primary.SpreadMovement = 0.3
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Delay = 0.1
SWEP.Primary.Force = 1

SWEP.Secondary.ClipSize = 0
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

SWEP.Base = "wos_dods_weapon_base"
SWEP.HoldBase = "dods-30cal"
SWEP.HoldType = SWEP.HoldBase .. "-idle"
SWEP.Deployable = true