if CLIENT then
SWEP.WepSelectIcon = surface.GetTextureID( "sprites/hud/dods_mp44" )
SWEP.DrawWeaponInfoBox	= false
SWEP.BounceWeaponIcon = false
killicon.Add( "weapon_mp44", "sprites/hud/dods_mp44", Color( 255, 255, 255, 255 ) )
end

SWEP.PrintName = "MP44"
SWEP.Category = "Day of Defeat: Source"
SWEP.Spawnable= true
SWEP.AdminSpawnable= true
SWEP.AdminOnly = false

SWEP.ViewModelFOV = 45
SWEP.ViewModel = "models/weapons/c_mp44.mdl"
SWEP.WorldModel = "models/weapons/w_mp44.mdl"
SWEP.ViewModelFlip = false

SWEP.AutoSwitchTo = false
SWEP.AutoSwitchFrom = false
SWEP.Weight = 20
SWEP.Slot = 2
SWEP.SlotPos = 1

SWEP.UseHands = true
SWEP.FiresUnderwater = true
SWEP.DrawCrosshair = false
SWEP.DrawAmmo = true
SWEP.CSMuzzleFlashes = 1

SWEP.FireMode = 0
SWEP.FireModeForward = 3
SWEP.FireModeRight = -2
SWEP.FireModeUp = 3.5
SWEP.Sprint = 0
SWEP.SprintAngles = 0
SWEP.Reloading = 0
SWEP.ReloadingTimer = CurTime()
SWEP.Idle = 0
SWEP.IdleTimer = CurTime()
SWEP.Recoil = 0
SWEP.RecoilTimer = CurTime()
SWEP.RecoilDirection = 0

SWEP.Primary.Sound = Sound( "Weapon_MP44.Shoot" )
SWEP.Primary.ClipSize = 30
SWEP.Primary.DefaultClip = 210
SWEP.Primary.MaxAmmo = 180
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "AR2"
SWEP.Primary.Damage = 50
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.Spread = 0.025
SWEP.Primary.SpreadSecondary = 0.02
SWEP.Primary.SpreadMovement = 0.125
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Delay = 0.12
SWEP.Primary.DelaySecondary = 0.3
SWEP.Primary.Force = 1

SWEP.Secondary.ClipSize = 0
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

SWEP.Base = "wos_dods_weapon_base"
SWEP.HoldBase = "dods-mp44"
SWEP.HoldType = SWEP.HoldBase .. "-idle"
SWEP.SingleSecondary = true

SWEP.FireModeScale = -0.1
SWEP.FireModeForward = 2.5
SWEP.FireModeRight = 1
SWEP.FireModeUp = 0.25

SWEP.FireForwardScale = 2
SWEP.FireUpScale = 0.42
SWEP.FireRightScale = 0.92

SWEP.FireForwardMin = -3.5
SWEP.FireForwardMax = 2.5

SWEP.FireUpMin = 0.25
SWEP.FireUpMax = 1.5

SWEP.FireRightMin = -1.75
SWEP.FireRightMax = 1