if CLIENT then
SWEP.WepSelectIcon = surface.GetTextureID( "sprites/hud/dods_c96" )
SWEP.DrawWeaponInfoBox	= false
SWEP.BounceWeaponIcon = false
killicon.Add( "weapon_c96", "sprites/hud/dods_c96", Color( 255, 255, 255, 255 ) )
end

SWEP.PrintName = "C96"
SWEP.Category = "Day of Defeat: Source"
SWEP.Spawnable= true
SWEP.AdminSpawnable= true
SWEP.AdminOnly = false

SWEP.ViewModelFOV = 45
SWEP.ViewModel = "models/weapons/c_c96.mdl"
SWEP.WorldModel = "models/weapons/w_c96.mdl"
SWEP.ViewModelFlip = false

SWEP.AutoSwitchTo = false
SWEP.AutoSwitchFrom = false
SWEP.Weight = 10
SWEP.Slot = 1
SWEP.SlotPos = 1

SWEP.UseHands = true
SWEP.HoldType = "ar2"
SWEP.FiresUnderwater = true
SWEP.DrawCrosshair = false
SWEP.DrawAmmo = true
SWEP.CSMuzzleFlashes = 1
SWEP.Base = "weapon_base"

SWEP.Sprint = 0
SWEP.SprintAngles = 0
SWEP.Reloading = 0
SWEP.ReloadingTimer = CurTime()
SWEP.Idle = 0
SWEP.IdleTimer = CurTime()
SWEP.Recoil = 0
SWEP.RecoilTimer = CurTime()
SWEP.RecoilDirection = 0

SWEP.Primary.Sound = Sound( "Weapon_C96.Shoot" )
SWEP.Primary.ClipSize = 20
SWEP.Primary.DefaultClip = 60
SWEP.Primary.MaxAmmo = 40
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "Pistol"
SWEP.Primary.Damage = 40
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.Spread = 0.065
SWEP.Primary.SpreadMovement = 0.165
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Delay = 0.065
SWEP.Primary.Force = 1

SWEP.Secondary.ClipSize = 0
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

SWEP.Base = "wos_dods_weapon_base"
SWEP.HoldBase = "dods-c96"
SWEP.HoldType = SWEP.HoldBase .. "-idle"