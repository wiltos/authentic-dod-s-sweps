if CLIENT then
SWEP.WepSelectIcon = surface.GetTextureID( "sprites/hud/dods_thompson" )
SWEP.DrawWeaponInfoBox	= false
SWEP.BounceWeaponIcon = false
killicon.Add( "weapon_thompson", "sprites/hud/dods_thompson", Color( 255, 255, 255, 255 ) )
end

SWEP.PrintName = "Thompson"
SWEP.Category = "Day of Defeat: Source"
SWEP.Spawnable= true
SWEP.AdminSpawnable= true
SWEP.AdminOnly = false

SWEP.ViewModelFOV = 45
SWEP.ViewModel = "models/weapons/c_thompson.mdl"
SWEP.WorldModel = "models/weapons/w_thompson.mdl"
SWEP.ViewModelFlip = false

SWEP.AutoSwitchTo = false
SWEP.AutoSwitchFrom = false
SWEP.Weight = 20
SWEP.Slot = 2
SWEP.SlotPos = 1

SWEP.UseHands = true
SWEP.HoldType = "ar2"
SWEP.FiresUnderwater = true
SWEP.DrawCrosshair = false
SWEP.DrawAmmo = true
SWEP.CSMuzzleFlashes = 1

SWEP.Attack = 0
SWEP.AttackTimer = CurTime()
SWEP.Sprint = 0
SWEP.SprintAngles = 0
SWEP.Reloading = 0
SWEP.ReloadingTimer = CurTime()
SWEP.Idle = 0
SWEP.IdleTimer = CurTime()
SWEP.Recoil = 0
SWEP.RecoilTimer = CurTime()
SWEP.RecoilDirection = 0

SWEP.Primary.Sound = Sound( "Weapon_Thompson.Shoot" )
SWEP.Primary.ClipSize = 30
SWEP.Primary.DefaultClip = 210
SWEP.Primary.MaxAmmo = 180
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "SMG1"
SWEP.Primary.Damage = 40
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.Spread = 0.055
SWEP.Primary.SpreadMovement = 0.155
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Delay = 0.085
SWEP.Primary.Force = 1

SWEP.Secondary.ClipSize = 0
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"
SWEP.Secondary.Damage = 60
SWEP.Secondary.Delay = 0.4
SWEP.Secondary.Force = 1000

SWEP.Base = "wos_dods_weapon_base"
SWEP.HoldBase = "dods-tommy"
SWEP.HoldType = SWEP.HoldBase .. "-idle"
SWEP.MeleeSecondary = true