if CLIENT then
SWEP.WepSelectIcon = surface.GetTextureID( "sprites/hud/dods_riflegren_us" )
SWEP.DrawWeaponInfoBox	= false
SWEP.BounceWeaponIcon = false
killicon.Add( "weapon_riflegren_us", "sprites/hud/dods_riflegren_us", Color( 255, 255, 255, 255 ) )
killicon.Add( "ent_riflegren_us", "sprites/hud/dods_riflegren_us", Color( 255, 255, 255, 255 ) )
end

SWEP.PrintName = "Garand Grenade"
SWEP.Category = "Day of Defeat: Source"
SWEP.Spawnable= true
SWEP.AdminSpawnable= true
SWEP.AdminOnly = false

SWEP.ViewModelFOV = 45
SWEP.ViewModel = "models/weapons/c_garand_rg.mdl"
SWEP.WorldModel = "models/weapons/wos/w_garand_gren.mdl"
SWEP.ViewModelFlip = false

SWEP.AutoSwitchTo = false
SWEP.AutoSwitchFrom = false
SWEP.Weight = 5
SWEP.Slot = 4
SWEP.SlotPos = 1

SWEP.UseHands = true
SWEP.HoldType = "ar2"
SWEP.FiresUnderwater = true
SWEP.DrawCrosshair = false
SWEP.DrawAmmo = true
SWEP.CSMuzzleFlashes = 1
SWEP.Base = "weapon_base"

SWEP.Sprint = 0
SWEP.SprintAngles = 0
SWEP.Reloading = 0
SWEP.ReloadingTimer = CurTime()
SWEP.Idle = 0
SWEP.IdleTimer = CurTime()
SWEP.Recoil = 0
SWEP.RecoilTimer = CurTime()
SWEP.RecoilDirection = 0

SWEP.Primary.Sound = Sound( "Weapon_Grenade.Shoot" )
SWEP.Primary.ClipSize = 1
SWEP.Primary.DefaultClip = 2
SWEP.Primary.MaxAmmo = 2
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "SMG1_Grenade"
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.Delay = 1
SWEP.Primary.Force = 6500

SWEP.Secondary.ClipSize = 0
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

SWEP.Base = "wos_dods_weapon_base"
SWEP.HoldBase = "dods-riflegren"
SWEP.HoldType = SWEP.HoldBase .. "-idle"
SWEP.GarGrenade = true