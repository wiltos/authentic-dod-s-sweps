if CLIENT then
SWEP.WepSelectIcon = surface.GetTextureID( "sprites/hud/dods_bazooka" )
SWEP.DrawWeaponInfoBox	= false
SWEP.BounceWeaponIcon = false
killicon.Add( "weapon_bazooka", "sprites/hud/dods_bazooka", Color( 255, 255, 255, 255 ) )
killicon.Add( "ent_bazooka_rocket", "sprites/hud/dods_bazooka", Color( 255, 255, 255, 255 ) )
end

SWEP.PrintName = "Bazooka"
SWEP.Category = "Day of Defeat: Source"
SWEP.Spawnable= true
SWEP.AdminSpawnable= true
SWEP.AdminOnly = false

SWEP.ViewModelFOV = 45
SWEP.ViewModel = "models/weapons/c_bazooka.mdl"
SWEP.WorldModel = "models/weapons/w_bazooka.mdl"
SWEP.ViewModelFlip = false

SWEP.AutoSwitchTo = false
SWEP.AutoSwitchFrom = false
SWEP.Weight = 20
SWEP.Slot = 2
SWEP.SlotPos = 1

SWEP.UseHands = true
SWEP.HoldType = "rpg"
SWEP.FiresUnderwater = false
SWEP.DrawCrosshair = false
SWEP.DrawAmmo = true
SWEP.CSMuzzleFlashes = 1
SWEP.Base = "weapon_base"

SWEP.Aim = 0
SWEP.Sprint = 0
SWEP.SprintAngles = 0
SWEP.Reloading = 0
SWEP.ReloadingTimer = CurTime()
SWEP.Idle = 0
SWEP.IdleTimer = CurTime()
SWEP.Recoil = 0
SWEP.RecoilTimer = CurTime()
SWEP.RecoilDirection = 0

SWEP.Primary.Sound = Sound( "Weapon_Bazooka.Shoot" )
SWEP.Primary.ClipSize = 1
SWEP.Primary.DefaultClip = 5
SWEP.Primary.MaxAmmo = 4
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "RPG_Round"
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.Delay = 1
SWEP.Primary.Force = 9999

SWEP.Secondary.ClipSize = 0
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

SWEP.Base = "wos_dods_weapon_base"
SWEP.HoldBase = "dods-bazooka"
SWEP.HoldType = SWEP.HoldBase .. "-idle"
SWEP.BazookaRocket = true