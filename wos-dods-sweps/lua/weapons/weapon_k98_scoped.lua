if CLIENT then
SWEP.WepSelectIcon = surface.GetTextureID( "sprites/hud/dods_k98_scoped" )
SWEP.DrawWeaponInfoBox	= false
SWEP.BounceWeaponIcon = false
killicon.Add( "weapon_k98_scoped", "sprites/hud/dods_k98_scoped", Color( 255, 255, 255, 255 ) )
end

SWEP.PrintName = "K98 Scoped"
SWEP.Category = "Day of Defeat: Source"
SWEP.Spawnable= true
SWEP.AdminSpawnable= true
SWEP.AdminOnly = false

SWEP.ViewModelFOV = 45
SWEP.ViewModel = "models/weapons/c_k98_scoped.mdl"
SWEP.WorldModel = "models/weapons/wos/w_k98s.mdl"
SWEP.ViewModelFlip = false
SWEP.BobScale = 1
SWEP.SwayScale = 1

SWEP.AutoSwitchTo = false
SWEP.AutoSwitchFrom = false
SWEP.Weight = 20
SWEP.Slot = 2
SWEP.SlotPos = 1

SWEP.UseHands = true
SWEP.HoldType = "ar2"
SWEP.FiresUnderwater = true
SWEP.DrawCrosshair = false
SWEP.DrawAmmo = true
SWEP.CSMuzzleFlashes = 1
SWEP.Base = "weapon_base"

SWEP.Aim = 0
SWEP.AimForward = -4.3
SWEP.AimRight = -1.3
SWEP.AimUp = 1.2
SWEP.AimTimer = CurTime()
SWEP.AimTimerFOV = 0
SWEP.Sprint = 0
SWEP.SprintAngles = 0
SWEP.Reloading = 0
SWEP.ReloadingTimer = CurTime()
SWEP.Idle = 0
SWEP.IdleTimer = CurTime()
SWEP.Recoil = 0
SWEP.RecoilTimer = CurTime()
SWEP.RecoilDirection = 0

SWEP.Primary.Sound = Sound( "Weapon_KarScoped.Shoot" )
SWEP.Primary.ClipSize = 5
SWEP.Primary.DefaultClip = 65
SWEP.Primary.MaxAmmo = 60
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "AR2"
SWEP.Primary.Damage = 120
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.Spread = 0.06
SWEP.Primary.SpreadSecondary = 0
SWEP.Primary.SpreadMovement = 0.16
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Delay = 1.6
SWEP.Primary.Force = 1

SWEP.Secondary.ClipSize = 0
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"
SWEP.Secondary.Delay = 0.5

SWEP.Base = "wos_dods_weapon_base"
SWEP.HoldBase = "dods-bolt"
SWEP.HoldType = SWEP.HoldBase .. "-idle"
SWEP.KarScope = true
SWEP.BlockDODCrosshair = true

SWEP.AimForward = -4.3
SWEP.AimRight = -2.5
SWEP.AimUp = 1.2

SWEP.FireModeScale = -0.1

SWEP.FireForwardScale = 2
SWEP.FireUpScale = 0.38
SWEP.FireRightScale = 0.66

SWEP.FireForwardMin = -11
SWEP.FireForwardMax = -4.3

SWEP.FireUpMin = 1.2
SWEP.FireUpMax = 2.5

SWEP.FireRightMin = -3.5
SWEP.FireRightMax = -1.3