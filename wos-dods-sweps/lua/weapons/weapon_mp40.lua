SWEP.PrintName = "MP40"
SWEP.Category = "Day of Defeat: Source"
SWEP.Spawnable= true
SWEP.AdminSpawnable= true
SWEP.AdminOnly = false

SWEP.ViewModelFOV = 45
SWEP.ViewModel = "models/weapons/c_mp40.mdl"
SWEP.WorldModel = "models/weapons/w_mp40.mdl"
SWEP.ViewModelFlip = false

SWEP.AutoSwitchTo = false
SWEP.AutoSwitchFrom = false
SWEP.Weight = 20
SWEP.Slot = 2
SWEP.SlotPos = 1

SWEP.UseHands = true
SWEP.FiresUnderwater = true
SWEP.DrawCrosshair = false
SWEP.DrawAmmo = true
SWEP.CSMuzzleFlashes = 1


SWEP.Primary.Sound = Sound( "Weapon_MP40.Shoot" )
SWEP.Primary.ClipSize = 30
SWEP.Primary.DefaultClip = 210
SWEP.Primary.MaxAmmo = 180
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "SMG1"
SWEP.Primary.Damage = 40
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.Spread = 0.055
SWEP.Primary.SpreadMovement = 0.155
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Delay = 0.09
SWEP.Primary.Force = 1

SWEP.Secondary.ClipSize = 0
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"
SWEP.Secondary.Damage = 60
SWEP.Secondary.Delay = 0.4
SWEP.Secondary.Force = 1000

if CLIENT then
    SWEP.WepSelectIcon = surface.GetTextureID( "sprites/hud/dods_mp40" )
    SWEP.DrawWeaponInfoBox	= false
    SWEP.BounceWeaponIcon = false
    killicon.Add( "weapon_mp40", "sprites/hud/dods_mp40", Color( 255, 255, 255, 255 ) )
end

SWEP.Base = "wos_dods_weapon_base"
SWEP.HoldBase = "dods-mp40"
SWEP.HoldType = SWEP.HoldBase .. "-idle"
SWEP.MeleeSecondary = true