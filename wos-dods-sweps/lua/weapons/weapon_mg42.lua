if CLIENT then
SWEP.WepSelectIcon = surface.GetTextureID( "sprites/hud/dods_mg42" )
SWEP.DrawWeaponInfoBox	= false
SWEP.BounceWeaponIcon = false
killicon.Add( "weapon_mg42", "sprites/hud/dods_mg42", Color( 255, 255, 255, 255 ) )
end

SWEP.PrintName = "MG42"
SWEP.Category = "Day of Defeat: Source"
SWEP.Spawnable= true
SWEP.AdminSpawnable= true
SWEP.AdminOnly = false

SWEP.ViewModelFOV = 45
SWEP.ViewModel = "models/weapons/c_mg42.mdl"
SWEP.WorldModel = "models/weapons/w_mg42bd.mdl"
SWEP.ViewModelFlip = false

SWEP.AutoSwitchTo = false
SWEP.AutoSwitchFrom = false
SWEP.Weight = 20
SWEP.Slot = 2
SWEP.SlotPos = 1

SWEP.UseHands = true
SWEP.HoldType = "ar2"
SWEP.FiresUnderwater = true
SWEP.DrawCrosshair = false
SWEP.DrawAmmo = true
SWEP.CSMuzzleFlashes = 1
SWEP.Base = "weapon_base"

SWEP.Bipods = 0
SWEP.Sprint = 0
SWEP.SprintAngles = 0
SWEP.Reloading = 0
SWEP.ReloadingTimer = CurTime()
SWEP.Idle = 0
SWEP.IdleTimer = CurTime()
SWEP.Recoil = 0
SWEP.RecoilTimer = CurTime()
SWEP.RecoilDirection = 0

SWEP.Primary.Sound = Sound( "Weapon_Mg42.Shoot" )
SWEP.Primary.ClipSize = 250
SWEP.Primary.DefaultClip = 500
SWEP.Primary.MaxAmmo = 250
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "AR2"
SWEP.Primary.Damage = 85
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.Spread = 0.2
SWEP.Primary.SpreadSecondary = 0.025
SWEP.Primary.SpreadMovement = 0.3
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Delay = 0.05
SWEP.Primary.Force = 1

SWEP.Secondary.ClipSize = 0
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

SWEP.Base = "wos_dods_weapon_base"
SWEP.HoldBase = "dods-mg"
SWEP.HoldType = SWEP.HoldBase .. "-idle"
SWEP.Deployable = true
SWEP.MG42Deployable = true