if CLIENT then
SWEP.WepSelectIcon = surface.GetTextureID( "sprites/hud/dods_garand" )
SWEP.DrawWeaponInfoBox	= false
SWEP.BounceWeaponIcon = false
killicon.Add( "weapon_garand", "sprites/hud/dods_garand", Color( 255, 255, 255, 255 ) )
end

SWEP.PrintName = "Garand"
SWEP.Category = "Day of Defeat: Source"
SWEP.Spawnable= true
SWEP.AdminSpawnable= true
SWEP.AdminOnly = false

SWEP.ViewModelFOV = 45
SWEP.ViewModel = "models/weapons/c_garand.mdl"
SWEP.WorldModel = "models/weapons/wos/w_garand.mdl"
SWEP.ViewModelFlip = false
SWEP.BobScale = 1
SWEP.SwayScale = 1

SWEP.AutoSwitchTo = false
SWEP.AutoSwitchFrom = false
SWEP.Weight = 20
SWEP.Slot = 2
SWEP.SlotPos = 1

SWEP.UseHands = true
SWEP.HoldType = "ar2"
SWEP.FiresUnderwater = true
SWEP.DrawCrosshair = false
SWEP.DrawAmmo = true
SWEP.CSMuzzleFlashes = 1
SWEP.Base = "weapon_base"

SWEP.Aim = 0
SWEP.AimForward = 2
SWEP.AimRight = -3.3
SWEP.AimUp = 3.5
SWEP.AimTimer = CurTime()
SWEP.AimTimerFOV = 0
SWEP.Sprint = 0
SWEP.SprintAngles = 0
SWEP.Reloading = 0
SWEP.ReloadingTimer = CurTime()
SWEP.Idle = 0
SWEP.IdleTimer = CurTime()
SWEP.Recoil = 0
SWEP.RecoilTimer = CurTime()
SWEP.RecoilDirection = 0

SWEP.Primary.Sound = Sound( "Weapon_Garand.Shoot" )
SWEP.Primary.ClipSize = 8
SWEP.Primary.DefaultClip = 88
SWEP.Primary.MaxAmmo = 80
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "AR2"
SWEP.Primary.Damage = 80
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.Spread = 0.014
SWEP.Primary.SpreadSecondary = 0
SWEP.Primary.SpreadMovement = 0.114
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Delay = 0.37
SWEP.Primary.Force = 1

SWEP.Secondary.ClipSize = 0
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"
SWEP.Secondary.Delay = 0.5

SWEP.Base = "wos_dods_weapon_base"
SWEP.HoldBase = "dods-rifle"
SWEP.HoldType = SWEP.HoldBase .. "-idle"
SWEP.IronSights = true
SWEP.BlockDODCrosshair = true
SWEP.GarandReload = true

SWEP.AimForward = -4.3
SWEP.AimRight = -2.5
SWEP.AimUp = 1.2

SWEP.FireModeScale = -0.1

SWEP.FireForwardScale = 0.19
SWEP.FireUpScale = 0.63
SWEP.FireRightScale = 1

SWEP.FireForwardMin = 2
SWEP.FireForwardMax = 5.9

SWEP.FireUpMin = 3.5
SWEP.FireUpMax = 5.04

SWEP.FireRightMin = -6.859
SWEP.FireRightMax =  -3.3