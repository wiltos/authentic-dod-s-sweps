
hook.Add("prone.Initialized", "wOS.DODS.CreateProneLayers", function()

    prone.Animations.WeaponAnims.idle[ "dods-mp40-idle" ] = "proneaim_mp40"
    prone.Animations.WeaponAnims.idle[ "dods-mp40-shoot" ] = prone.Animations.WeaponAnims.idle[ "dods-mp40-idle" ]
    prone.Animations.WeaponAnims.moving[ "dods-mp40-idle" ] = "pronewalkidle_mp40"
    prone.Animations.WeaponAnims.moving[ "dods-mp40-shoot" ] = prone.Animations.WeaponAnims.moving[ "dods-mp40-idle" ]

    prone.Animations.WeaponAnims.idle[ "dods-tommy-idle" ] = "proneaim_tommy"
    prone.Animations.WeaponAnims.idle[ "dods-tommy-shoot" ] = prone.Animations.WeaponAnims.idle[ "dods-tommy-idle" ]
    prone.Animations.WeaponAnims.moving[ "dods-tommy-idle" ] = "pronewalkidle_tommy"
    prone.Animations.WeaponAnims.moving[ "dods-tommy-shoot" ] = prone.Animations.WeaponAnims.moving[ "dods-tommy-idle" ]

    prone.Animations.WeaponAnims.idle[ "dods-mp44-idle" ] = "proneaim_mp44"
    prone.Animations.WeaponAnims.idle[ "dods-mp44-shoot" ] = prone.Animations.WeaponAnims.idle[ "dods-mp44-idle" ]
    prone.Animations.WeaponAnims.moving[ "dods-mp44-idle" ] = "pronewalkidle_mp44"
    prone.Animations.WeaponAnims.moving[ "dods-mp44-shoot" ] = prone.Animations.WeaponAnims.moving[ "dods-mp44-idle" ]

    prone.Animations.WeaponAnims.idle[ "dods-bar-idle" ] = "proneaim_bar"
    prone.Animations.WeaponAnims.idle[ "dods-bar-shoot" ] = prone.Animations.WeaponAnims.idle[ "dods-bar-idle" ]
    prone.Animations.WeaponAnims.moving[ "dods-bar-idle" ] = "pronewalkidle_bar"
    prone.Animations.WeaponAnims.moving[ "dods-bar-shoot" ] = prone.Animations.WeaponAnims.moving[ "dods-bar-idle" ]

    prone.Animations.WeaponAnims.idle[ "dods-rifle-idle" ] = "proneaim_rifle"
    prone.Animations.WeaponAnims.idle[ "dods-rifle-shoot" ] = prone.Animations.WeaponAnims.idle[ "dods-rifle-idle" ]
    prone.Animations.WeaponAnims.moving[ "dods-rifle-idle" ] = "pronewalkidle_rifle"
    prone.Animations.WeaponAnims.moving[ "dods-rifle-shoot" ] = prone.Animations.WeaponAnims.moving[ "dods-rifle-idle" ]

    prone.Animations.WeaponAnims.idle[ "dods-bolt-idle" ] = "proneaim_bolt"
    prone.Animations.WeaponAnims.idle[ "dods-bolt-shoot" ] = prone.Animations.WeaponAnims.idle[ "dods-bolt-idle" ]
    prone.Animations.WeaponAnims.moving[ "dods-bolt-idle" ] = "pronewalkidle_bolt"
    prone.Animations.WeaponAnims.moving[ "dods-bolt-shoot" ] = prone.Animations.WeaponAnims.moving[ "dods-bolt-idle" ]

    prone.Animations.WeaponAnims.idle[ "dods-pistol-idle" ] = "proneaim_pistol"
    prone.Animations.WeaponAnims.idle[ "dods-pistol-shoot" ] = prone.Animations.WeaponAnims.idle[ "dods-pistol-idle" ]
    prone.Animations.WeaponAnims.moving[ "dods-pistol-idle" ] = "pronewalkidle_pistol"
    prone.Animations.WeaponAnims.moving[ "dods-pistol-shoot" ] = prone.Animations.WeaponAnims.moving[ "dods-pistol-idle" ]

    prone.Animations.WeaponAnims.idle[ "dods-c96-idle" ] = "proneaim_c96"
    prone.Animations.WeaponAnims.idle[ "dods-c96-shoot" ] = prone.Animations.WeaponAnims.idle[ "dods-c96-idle" ]
    prone.Animations.WeaponAnims.moving[ "dods-c96-idle" ] = "pronewalkidle_c96"
    prone.Animations.WeaponAnims.moving[ "dods-c96-shoot" ] = prone.Animations.WeaponAnims.moving[ "dods-c96-idle" ]

    prone.Animations.WeaponAnims.idle[ "dods-m1carbine-idle" ] = "proneaim_rifle"
    prone.Animations.WeaponAnims.idle[ "dods-m1carbine-shoot" ] = prone.Animations.WeaponAnims.idle[ "dods-m1carbine-idle" ]
    prone.Animations.WeaponAnims.moving[ "dods-m1carbine-idle" ] = "pronewalkidle_rifle"
    prone.Animations.WeaponAnims.moving[ "dods-m1carbine-shoot" ] = prone.Animations.WeaponAnims.moving[ "dods-m1carbine-idle" ]

    prone.Animations.WeaponAnims.idle[ "dods-bazooka-idle" ] = "proneaim_bazooka"
    prone.Animations.WeaponAnims.idle[ "dods-bazooka-shoot" ] = prone.Animations.WeaponAnims.idle[ "dods-bazooka-idle" ]
    prone.Animations.WeaponAnims.moving[ "dods-bazooka-idle" ] = "pronewalkidle_bazooka"
    prone.Animations.WeaponAnims.moving[ "dods-bazooka-shoot" ] = prone.Animations.WeaponAnims.moving[ "dods-bazooka-idle" ]

    prone.Animations.WeaponAnims.idle[ "dods-pschreck-idle" ] = "proneaim_pschreck"
    prone.Animations.WeaponAnims.idle[ "dods-pschreck-shoot" ] = prone.Animations.WeaponAnims.idle[ "dods-pschreck-idle" ]
    prone.Animations.WeaponAnims.moving[ "dods-pschreck-idle" ] = "pronewalkidle_pschreck"
    prone.Animations.WeaponAnims.moving[ "dods-pschreck-shoot" ] = prone.Animations.WeaponAnims.moving[ "dods-pschreck-idle" ]

    prone.Animations.WeaponAnims.idle[ "dods-mg-idle" ] = "proneaim_mg"
    prone.Animations.WeaponAnims.idle[ "dods-mg-shoot" ] = prone.Animations.WeaponAnims.idle[ "dods-mg-idle" ]
    prone.Animations.WeaponAnims.moving[ "dods-mg-idle" ] = "pronewalkidle_mg"
    prone.Animations.WeaponAnims.moving[ "dods-mg-shoot" ] = prone.Animations.WeaponAnims.moving[ "dods-mg-idle" ]
    prone.Animations.WeaponAnims.idle[ "dods-mg-deploy" ] = prone.Animations.WeaponAnims.idle[ "dods-mg-idle" ]
    prone.Animations.WeaponAnims.moving[ "dods-mg-deploy" ] = prone.Animations.WeaponAnims.moving[ "dods-mg-idle" ]

    prone.Animations.WeaponAnims.idle[ "dods-30cal-idle" ] = "proneaim_30cal"
    prone.Animations.WeaponAnims.idle[ "dods-30cal-shoot" ] = prone.Animations.WeaponAnims.idle[ "dods-30cal-idle" ]
    prone.Animations.WeaponAnims.moving[ "dods-30cal-idle" ] = "pronewalkidle_30cal"
    prone.Animations.WeaponAnims.moving[ "dods-30cal-shoot" ] = prone.Animations.WeaponAnims.moving[ "dods-30cal-idle" ]
    prone.Animations.WeaponAnims.idle[ "dods-30cal-deploy" ] = prone.Animations.WeaponAnims.idle[ "dods-30cal-idle" ]
    prone.Animations.WeaponAnims.moving[ "dods-30cal-deploy" ] = prone.Animations.WeaponAnims.moving[ "dods-30cal-idle" ]

    prone.Animations.WeaponAnims.idle[ "dods-riflegren-idle" ] = "proneaim_rifle"
    prone.Animations.WeaponAnims.idle[ "dods-riflegren-shoot" ] = prone.Animations.WeaponAnims.idle[ "dods-riflegren-idle" ]
    prone.Animations.WeaponAnims.moving[ "dods-riflegren-idle" ] = "pronewalkidle_rifle"
    prone.Animations.WeaponAnims.moving[ "dods-riflegren-shoot" ] = prone.Animations.WeaponAnims.moving[ "dods-riflegren-idle" ]

    prone.Animations.WeaponAnims.idle[ "dods-knife-idle" ] = "proneaim_knife"
    prone.Animations.WeaponAnims.idle[ "dods-knife-shoot" ] = prone.Animations.WeaponAnims.idle[ "dods-knife-idle" ]
    prone.Animations.WeaponAnims.moving[ "dods-knife-idle" ] = "pronewalkaim_knife"
    prone.Animations.WeaponAnims.moving[ "dods-knife-shoot" ] = prone.Animations.WeaponAnims.moving[ "dods-knife-idle" ]

    prone.Animations.WeaponAnims.idle[ "dods-spade-idle" ] = "proneaim_spade"
    prone.Animations.WeaponAnims.idle[ "dods-spade-shoot" ] = prone.Animations.WeaponAnims.idle[ "dods-spade-idle" ]
    prone.Animations.WeaponAnims.moving[ "dods-spade-idle" ] = "pronewalkaim_spade"
    prone.Animations.WeaponAnims.moving[ "dods-spade-shoot" ] = prone.Animations.WeaponAnims.moving[ "dods-spade-idle" ]

end )