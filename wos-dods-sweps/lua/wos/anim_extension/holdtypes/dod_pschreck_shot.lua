--=====================================================================
/*		My Custom Holdtype
			Created by King David™( STEAM_0:0:13757046 )*/
local DATA = {}
DATA.Name = "Day of Defeat: Source Panzerschreck (Shoot)"
DATA.HoldType = "dods-pschreck-shoot"
DATA.BaseHoldType = "rpg"
DATA.Translations = {} 

DATA.Translations[ ACT_MP_STAND_IDLE ] = {
	{ Sequence = "standaim_pschreck", Weight = 1 },
}

DATA.Translations[ ACT_MP_ATTACK_STAND_PRIMARYFIRE ] = {
	{ Sequence = "attack_pschreck", Weight = 1 },
}

DATA.Translations[ ACT_MP_ATTACK_CROUCH_PRIMARYFIRE ] = {
	{ Sequence = "attack_pschreck", Weight = 1 },
}

DATA.Translations[ ACT_MP_CROUCHWALK ] = {
	{ Sequence = "c_crouchwalkaim_pschreck", Weight = 1 },
}

DATA.Translations[ ACT_MP_WALK ] = {
	{ Sequence = "r_runaim_pschreck", Weight = 1 },
}

-- DATA.Translations[ ACT_MP_RELOAD_STAND ] = {
-- 	{ Sequence = "reload_pschreck", Weight = 1 },
-- }

-- DATA.Translations[ ACT_MP_RELOAD_CROUCH ] = {
-- 	{ Sequence = "reloadcrouch_pschreck", Weight = 1 },
-- }

DATA.Translations[ ACT_MP_CROUCH_IDLE ] = {
	{ Sequence = "crouchaim_pschreck", Weight = 1 },
}

DATA.Translations[ ACT_MP_RUN ] = {
	{ Sequence = "r_runaim_pschreck", Weight = 1 },
}

DATA.Translations[ ACT_MP_SPRINT ] = {
	{ Sequence = "s_sprintidle_pschreck", Weight = 1 },
}

DATA.Translations[ ACT_MP_JUMP ] = nil

wOS.AnimExtension:RegisterHoldtype( DATA )
--=====================================================================
