AddCSLuaFile()
ENT.Type = "anim"
ENT.Base = "base_anim"
ENT.Spawnable = false

function ENT:Draw()
self.Entity:DrawModel()
end

function ENT:Initialize()
self.Entity:SetModel( "models/weapons/w_garand_rg_grenade.mdl" )
self.Entity:SetMoveType( MOVETYPE_VPHYSICS )
self.Entity:SetSolid( SOLID_VPHYSICS )
self.Entity:PhysicsInit( SOLID_VPHYSICS )
self.Entity:SetCollisionGroup( COLLISION_GROUP_WEAPON )
self.Entity:DrawShadow( false )
self.ExplodeTimer = CurTime() + 3
end

function ENT:Think()
EffectData():SetOrigin( self:GetPos() )
util.Effect( "effect_riflegren_smoke", EffectData() )
if SERVER and self.ExplodeTimer <= CurTime() then
self.Entity:Remove()
end
end

function ENT:PhysicsCollide( data )
if data.Speed > 50 then
self.Entity:EmitSound( "Grenade.ImpactHard" )
end
end

function ENT:OnRemove()
if SERVER then
    local explode = ents.Create( "env_explosion" )
    explode:SetOwner( self.Owner )
    explode:SetPos( self:GetPos() )
    explode:Spawn()
    explode:Fire( "Explode", 0, 0 )
    self:EmitSound( "weapons/dod_explode" .. math.random( 3, 5 ) .. ".wav" )
end
util.BlastDamage( self, self.Owner, self:GetPos(), 512, 141 )
end