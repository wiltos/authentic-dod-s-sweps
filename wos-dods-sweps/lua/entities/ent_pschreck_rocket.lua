AddCSLuaFile()
ENT.Type = "anim"
ENT.Base = "base_anim"
ENT.Spawnable = false

function ENT:Draw()
self.Entity:DrawModel()
end

function ENT:Initialize()
self.Entity:SetModel( "models/weapons/w_panzerschreck_rocket.mdl" )
self.Entity:SetMoveType( MOVETYPE_VPHYSICS )
self.Entity:SetSolid( SOLID_VPHYSICS )
self.Entity:PhysicsInit( SOLID_VPHYSICS )
self.Entity:SetCollisionGroup( COLLISION_GROUP_INTERACTIVE )
self.Entity:DrawShadow( false )
end

function ENT:Think()
EffectData():SetOrigin( self:GetPos() )
util.Effect( "effect_rocket_smoke", EffectData() )
if SERVER and self.Entity:WaterLevel() > 0 then
self.Entity:Remove()
end
end

function ENT:PhysicsCollide()
if SERVER then
self.Entity:Remove()
end
end

function ENT:OnRemove()
if SERVER then
local explode = ents.Create( "env_explosion" )
explode:SetOwner( self.Owner )
explode:SetPos( self:GetPos() )
explode:Spawn()
explode:Fire( "Explode", 0, 0 )
end
util.BlastDamage( self, self.Owner, self:GetPos(), 512, 127 )
end