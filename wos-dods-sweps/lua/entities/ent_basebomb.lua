AddCSLuaFile()
ENT.Type = "anim"
ENT.Base = "base_anim"
ENT.Spawnable = false

function ENT:Draw()
self.Entity:DrawModel()
end

function ENT:Initialize()
self.Entity:SetModel( "models/weapons/w_tnt.mdl" )
self.Entity:SetMoveType( MOVETYPE_VPHYSICS )
self.Entity:SetSolid( SOLID_VPHYSICS )
self.Entity:PhysicsInit( SOLID_VPHYSICS )
self.Entity:SetCollisionGroup( COLLISION_GROUP_WEAPON )
self.Entity:DrawShadow( false )
self.ExplodeTimer = CurTime() + 20
self.Entity:EmitSound( "Weapon_C4.Fuse" )
end

function ENT:Think()
if SERVER and self.ExplodeTimer <= CurTime() then
self.Entity:Remove()
end
end

function ENT:PhysicsCollide( data )
if data.Speed > 50 then
self.Entity:EmitSound( "Weapon_C4.PickUp" )
end
end

function ENT:OnRemove()
if SERVER then
local explode = ents.Create( "env_explosion" )
explode:SetOwner( self.Owner )
explode:SetPos( self:GetPos() )
explode:Spawn()
explode:Fire( "Explode", 0, 0 )
end
self.Entity:EmitSound( "Weapon_C4.Explode" )
util.BlastDamage( self, self.Owner, self:GetPos(), 512, 241 )
end