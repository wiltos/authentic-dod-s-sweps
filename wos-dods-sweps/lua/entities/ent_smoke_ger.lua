AddCSLuaFile()
ENT.Type = "anim"
ENT.Base = "base_anim"
ENT.Spawnable = false

function ENT:Draw()
self.Entity:DrawModel()
end

function ENT:Initialize()
self.Entity:SetModel( "models/weapons/w_smoke_ger.mdl" )
self.Entity:SetMoveType( MOVETYPE_VPHYSICS )
self.Entity:SetSolid( SOLID_VPHYSICS )
self.Entity:PhysicsInit( SOLID_VPHYSICS )
self.Entity:SetCollisionGroup( COLLISION_GROUP_WEAPON )
self.Entity:DrawShadow( false )
self.Explode = 0
self.ExplodeTimer = CurTime() + 2
end

function ENT:Think()
if self.Explode == 0 then
end
if self.Explode == 0 and self.ExplodeTimer <= CurTime() then
if SERVER then
for i = 0, ( 5 ) do
smoke = ents.Create( "env_smoketrail" )
smoke:SetKeyValue( "startsize", "640" )
smoke:SetKeyValue( "endsize", "512" )
smoke:SetKeyValue( "spawnradius", "196" )
smoke:SetKeyValue( "opacity", "1" )
smoke:SetKeyValue( "spawnrate", "10" )
smoke:SetKeyValue( "lifetime", "15" )
smoke:SetPos( self:GetPos() )
smoke:SetParent( self.Entity )
smoke:Spawn()
smoke:Fire( "kill", "", 15 )
end
self.Entity:EmitSound( "BaseSmokeEffect.Sound" )
end
self.Explode = 1
end
end

function ENT:PhysicsCollide( data )
if data.Speed > 50 then
self.Entity:EmitSound( "Grenade.ImpactHard" )
end
end